from django.urls import include, path
from django.contrib import admin
from django.contrib.auth import views as auth_views
from osvald import views
from osvald import views as osvald
from django.conf.urls import handler404, handler500,handler403

urlpatterns = [

    path('admin/', admin.site.urls),
    path(r'', include('osvald.urls')),

]

handler404 = osvald.error_404
handler500 = osvald.error_500
handler403 = osvald.error_403

