import django_tables2 as tables
import django_filters
from .models import Module_table_collections,Activity
from django_tables2.export.views import ExportMixin
from django_tables2.utils import Accessor

class CollectionsTable(ExportMixin,tables.Table):
    export_formats = ['csv', 'xlsx']
    class Meta:
        model = Module_table_collections
        template_name = 'django_tables2/bootstrap.html'



class DetailTable(ExportMixin,tables.Table):
    export_formats = ['csv', 'xlsx']
    activity_count = tables.Column(verbose_name= 'Počet' )
    total_time = tables.Column(verbose_name= 'Celkový čas' )
    updated_at = tables.Column(verbose_name= 'Poslední změna' )
    created_at = tables.Column(verbose_name= 'Vytvořeno' )
    document_id = tables.Column(verbose_name= 'Číslo smlouvy' )
    customer_id = tables.Column(verbose_name= 'ID klienta' )
    template = tables.Column(verbose_name= 'Jméno aktivity', order_by=('template.name'))
    subtype = tables.Column(verbose_name= 'Podtyp', order_by=('activity_subtype.name'))
    comment = tables.Column(verbose_name= 'Komentář' )

    class Meta:
        model = Activity
        template_name = 'django_tables2/bootstrap.html'
        fields = ('username','customer_id','document_id','template','subtype','total_time','activity_count','created_at','updated_at','status')
        exclude = ('id','name','total_seconds',)


class DetailKapacityTable(ExportMixin,tables.Table):
    export_formats = ['csv', 'xlsx']
    mesic = tables.Column(verbose_name='Měsíc')
    username = tables.Column(verbose_name='Uživatel')
    fond = tables.Column(verbose_name='Odpracováno ANET (h)')
    cisty = tables.Column(verbose_name='Čistý čas (h)')
    efective = tables.Column(verbose_name='Efektivní čas (h)')
    podpurne = tables.Column(verbose_name='Podpůrné aktivity (h)')

    nepritomnost = tables.Column(verbose_name='Nepřítomnost (h)')

    ztrata = tables.Column(verbose_name='Ztráta efektivity (h)')

    class Meta:
        template_name = 'django_tables2/bootstrap.html'
