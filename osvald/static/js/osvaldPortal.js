//Function to change color tab
$(document).ready(function () {

    $(window).bind('beforeunload', function () {
        if (parseInt(localStorage.getItem("window-counter"), 10) > 1) {

            alert("Je povolena pouze jedna aktivní záložka pro aplikaci OSVALD");
            localStorage.setItem("window-counter", parseInt(localStorage.getItem("window-counter"), 10) - 1)
        } else {

            localStorage.setItem("window-counter", parseInt(0))
        }

    });

    $(window).ready(function () {

        if ((localStorage.getItem("window-counter") == undefined) || (localStorage.getItem("window-counter") == 0)) {
            localStorage.setItem("window-counter", 1);


            if (($(".active-activity-id").text() == localStorage.getItem("running-activity-id")) && ($(".active-activity-template-id").text() == localStorage.getItem('running-template-id')) && (localStorage.getItem('running-template-id') != undefined) && (localStorage.getItem('running-activity-timer') != undefined)) {

                activityContinue()

                //makeTestData()

            } else {
                if ($(".active-activity-template-id").text() != "") {
                    localStorage.setItem('running-template-id', $(".active-activity-template-id").text())
                }
                if ($(".active-activity-id").text() != "") {
                    localStorage.setItem('running-activity-id', $(".active-activity-id").text())
                }
                if (localStorage.getItem('running-activity-timer') != null) {
                    //activityContinue()
                    localStorage.removeItem('running-activity-timer')
                    localStorage.removeItem('running-activity-id')
                    localStorage.removeItem('unning-template-id')

                }
            }


        } else {
            if (localStorage.getItem("window-counter") >= 1) {
                localStorage.setItem("window-counter", parseInt(localStorage.getItem("window-counter"), 10) + 1)


                setTimeout(function () {
                    window.open('', '_self', '');
                    window.close();

                }, 1);
            }
        }
        return "Handler for .unload() called.";
    });


    if (localStorage.getItem("osvaldsession") != getCookie("csrftoken")) {
        localStorage.removeItem("running-activity-id");
        localStorage.removeItem('running-template-id');
        localStorage.removeItem('running-activity-timer');
        localStorage.setItem('osvaldsession', getCookie("csrftoken"))
    } else {
        localStorage.setItem('osvaldsession', getCookie("csrftoken"));
    }


    setInterval(function () {
        $.ajax({
            type: "GET",
            url: "/get_authorized/",
            async: false,
            cache: false,
            success: function (data) {

                if (data != "true") {
                    alert("Byl jste přihlášen z jiného místa. Toto okno se ukončí.")

                    setTimeout(function () {
                        localStorage.removeItem("window-counter");
                        window.open('', '_self', '');
                        window.close();
                        return false

                    }, 1);


                }


            }
            ,
            error: function (data) {
                alert("Pro dlouhodobou nečinnost, odhlášení z Windows nebo výpadek konektivity se toto okno ukončí.")

                    setTimeout(function () {
                        localStorage.removeItem("window-counter");
                        window.open('', '_self', '');
                        window.close();
                        return false

                    }, 1);
                return false


            }

        });
    }, 5000);//time in milliseconds





    if (($(".active-activity-id").text() != "") && (localStorage.getItem("running-activity-timer") == undefined)) {
        getTimeFromDB($(".active-activity-id").text())

    }


    $(window).unload(function () {

        return "Handler for .unload() called.";
    });


    $(function () {
        $("#id_updated_at_0").datetimepicker({
            format: 'YYYY-MM-DD'
        });
    });


    $(function () {
        $("#id_updated_at_1").datetimepicker({
            format: 'YYYY-MM-DD'
        });
    });


    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }


    $(".running-activity-top").click(function () {
        $(".tab-pane").removeClass("active");
        $(".nav-item").removeClass("active");

        var a = $('.panel-primary').find("span:contains('" + $(".active-activity-name").text() + "')").filter(function () {
            return $(this).text() === $(".active-activity-name").text();
        }).parent().parent().parent().parent().parent().parent();
        var b = $('.panel-primary').find("span:contains('" + $(".active-activity-name").text() + "')").filter(function () {
            return $(this).text() === $(".active-activity-name").text();
        }).parent().parent().parent().parent().parent().parent().attr("id");

        a.addClass("active");
        $("#" + b + "-tab").addClass("active");

        $("#" + b).addClass("active")
        $("#" + b).addClass("in")
        $("#" + b + "-tab").parent().addClass("active")
        var bg = $("#" + b + "-tab").css("background-color");

        $('.panel-activity').css({'background-color': bg});
        $('.panel-primary').css({'border-color': bg});
        $('.activity-tab-line').css({'border-color': bg});
    })

    var csrftoken = getCookie('csrftoken');
    $('[data-toggle="popover"]').popover();

    $(window).on("beforeunload", function () {
        // return confirm("Do you really want to close?");
    })

    $(".radio-single-activity").click(function () {
        $("#client-id").attr("disabled", true);
        $("#file-id").attr("disabled", true);
        $("#client-id").val("");
        $("#file-id").val("");
        if ($("input[name=single-activity]:checked").val() == "yes") {
            $("#client-id").attr("disabled", false);
            $("#file-id").attr("disabled", false);
        }
    });

    function updateActivityTime(time) {
        if (localStorage.getItem('running-activity-timer') != undefined) {
            $.ajaxSetup({
                headers: {"X-CSRFToken": getCookie("csrftoken")}
            });
            $.ajax({
                type: "POST",
                url: "/update_activity/",
                async: true,
                cache: false,
                data: {
                    'id': localStorage.getItem("running-activity-id"),
                    'total_time': time,
                    'csrfmiddlewaretoken': csrftoken,
                },

            });
        }
    }


    function getTimeFromDB(id) {
        console.log("In function " + Date.now());

        $.ajaxSetup({
            headers: {"X-CSRFToken": getCookie("csrftoken")}
        });
        console.log(id);
        $.ajax({
            type: "GET",
            url: "/get_time/",
            async: false,
            cache: false,
            data: {
                'id': id,
                'csrfmiddlewaretoken': csrftoken,
            },
            complete: function () {
                if (parseInt(localStorage.getItem("window-counter"), 10) < 2) {

                    //activityContinue();
                }
            },
            success: function (response) {
                if (response != "unauthorized") {
                    localStorage.setItem('running-activity-timer', response)
                    localStorage.setItem('running-template-id', $(".active-activity-template-id").text())
                    localStorage.setItem('running-activity-id', $(".active-activity-id").text())
                } else {

                    location.reload();
                }

            },
            error: function () {

                return false;
            }
        });


    }

    function checkRunningActivity() {

        if ($(".activity-timer").length > 0) {

            return true;
        }

        else {
            return false;
        }

    }

    function continueButtonClick(object) {
        return function () {

            if (checkRunningActivity()) {

                alert("Nejdříve ukončete spůštěnou aktivitu.")
            } else {
                $.ajaxSetup({
                    headers: {"X-CSRFToken": getCookie("csrftoken")}
                });
                $.ajax({
                    type: "POST",
                    url: "/continue_activity/",
                    async: true,
                    cache: false,
                    data: {
                        'id': object.find(".footer-hidden-id").text(),
                        'comment': $('#activity-continue-comment').val(),
                        'csrfmiddlewaretoken': csrftoken,
                    },
                    success: function (response) {
                        $(document).trigger("add-alerts", [
                            {
                                "message": "Aktivita  <b>'" + object.find(".footer-hidden-name").text() + "'</b> úspěšně obnovena.",
                                "priority": 'success'
                            }
                        ]);
                        if (response != "unauthorized") {
                            if (localStorage.getItem("running-activity-id") == undefined && localStorage.getItem("running-activity-timer") == undefined) {

                                //localStorage.removeItem('running-template-id');
                            }


                        }
                        location.reload()
                    },
                    error: function (rs, e) {
                        console.log("error " + rs.responseText);
                    }, complete: function () {


                    }
                });

            }
        }
    }

    function pauseButtonClick() {
        return function () {

            $.ajaxSetup({
                headers: {"X-CSRFToken": getCookie("csrftoken")}
            });
            $.ajax({
                type: "POST",
                url: "/pause_activity/",
                async: true,
                cache: false,
                data: {
                    'id': localStorage.getItem('running-activity-id'),
                    'comment': $('#activity-pause-comment').val(),
                    'total_time': localStorage.getItem('running-activity-timer'),
                    'csrfmiddlewaretoken': csrftoken,
                },
                success: function (response) {
                    $(document).trigger("add-alerts", [
                        {
                            "message": "Aktivita  <b>'" + $(".footer-hidden-name").text() + "'</b> úspěšně stopnuta.",
                            "priority": 'success'
                        }
                    ]);

                    localStorage.removeItem("running-activity-id");
                    localStorage.removeItem('running-template-id');
                    localStorage.removeItem('running-activity-timer');


                },
                error: function (rs, e) {
                    alert("error " + rs.responseText);
                },
                complete: function () {

                    location.reload();
                }
            });

        }
    }


    function stopButtonClick() {
        return function () {
            if ($('#activity-stop-count').val() != undefined) {

                var pocet = $('#activity-stop-count').val()
                if (isNaN(parseInt(pocet, 10))) {
                    alert("Zadejte počet kusů");
                    return false
                } else {
                    if (!(parseInt(pocet) > 0)) {
                        alert("Počet kusů musí být větší jak 0.");
                        return false
                    }
                }
            } else {
                var pocet = 1
            }
            if ($('.module-selector option:selected').attr("show_forms") == "True") {
                if ($('#collection-money').val() != undefined) {
                    var money = $('#modal-activity-stop').find('#collection-money').val();

                    if (isNaN(parseFloat(money, 10))) {
                        alert("Zadejte částku.");
                        return false
                    } else {
                        if (!(parseFloat(money) > 0)) {
                            alert("Částka musí být větší 0.");
                            return false
                        }
                    }
                } else {
                    money = ""
                }
                if ($('#collection-date').val() != undefined) {
                    var collDate = $('#modal-activity-stop').find('#collection-date').val()

                    if (collDate == "") {
                        alert("Zadejte datum.");
                        return false
                    } else {
                        var now = new Date()
                        var tstDay = collDate.split(".")[0];
                        var tstMonth = collDate.split(".")[1];
                        var tstYear = collDate.split(".")[2];

                        var nowYear = now.getFullYear()
                        var nowMonth = ("0" + (now.getMonth() + 1)).slice(-2)
                        var nowDay = ("0" + now.getDate()).slice(-2)



                        if (parseInt(tstYear + tstMonth + tstDay, 10) < parseInt(nowYear + nowMonth + nowDay, 10)) {
                            alert("Datum nesmí být v minulosti.")
                            return false
                        }
                    }
                } else {
                    collDate = ""
                }
            }else{
                var money = $('#modal-activity-stop').find('#collection-money').val();
                var collDate = $('#modal-activity-stop').find('#collection-date').val()

            }

            $.ajaxSetup({
                headers: {"X-CSRFToken": getCookie("csrftoken")}
            });

            $.ajax({
                type: "POST",
                url: "end_activity/",
                async: true,
                cache: false,
                data: {
                    'id': localStorage.getItem('running-activity-id'),
                    'total_time': localStorage.getItem('running-activity-timer'),
                    'comment': $('#modal-activity-stop').find('#activity-stop-comment').val(),
                    'activity_count': pocet,
                    'csrfmiddlewaretoken': csrftoken,
                    'value': parseFloat(money, 10),
                    'date_to_pay': collDate,
                    'description': $('#modal-activity-stop').find('#collection-description').val(),
                    'selector': $('#modal-activity-stop').find('.module-selector').val(),
                    'subtype': $('#modal-activity-stop').find('.subtype-selector').val(),
                },
                success: function (response) {
                    $(document).trigger("add-alerts", [
                        {
                            "message": "Aktivita  <b>'" + $(".modal-header").text() + "'</b> úspěšně ukončena.",
                            "priority": 'success'
                        }
                    ]);

                    localStorage.removeItem("running-activity-id");
                    localStorage.removeItem('running-template-id');
                    localStorage.removeItem('running-activity-timer');

                },
                error: function (rs, e) {
                    alert("error " + rs.responseText);
                }, complete: function () {

                    location.reload();
                }
            });

        }
    }


    function activityContinue() {
        activityID = $(".active-activity-template-id").text();

        activityTime = localStorage.getItem('running-activity-timer');


        var a = activityTime.split(":");
        var seconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2])
        var timer = new Timer();
        timer.start({startValues: {seconds: seconds}});
        var activityBox = $('.panel-primary').find("span:contains('" + $(".active-activity-name").text() + "')").filter(function () {
            return $(this).text() === $(".active-activity-name").text();
        }).parent().parent().parent();


        var timerElement = $(document.createElement('div'));
        timerElement.addClass("activity-timer");
        timerElement.addClass("activity-information-right");


        timer.addEventListener('secondsUpdated', function (e) {


            timerElement.text(timer.getTimeValues().toString());
            var avgText = activityBox.find(".activity-avg").text();

            var avg = new Date("January 1, 2000 " + avgText);
            avg = avg.getTime();

            var actual = new Date("January 1, 2000 " + timer.getTimeValues());
            actual = actual.getTime();
            if ((localStorage.getItem('running-activity-id') != null) && (localStorage.getItem('running-template-id') != null)) {
                localStorage.setItem('running-activity-timer', timer.getTimeValues().toString());
                $('.running-activity-top-time').text(timer.getTimeValues().toString())
            } else {

                //location.reload()
            }

            if (actual % 15000 == 0) {
                updateActivityTime(timer.getTimeValues().toString())
            }
            if (actual > avg) {

                timerElement.css({'color': 'red'});
            } else {
                timerElement.css({'color': 'white'});
            }

        });
        activityBox.find(".text-right").append(timerElement);
        var name = activityBox.find('h3.modal-header').text();
        var pauseElement = $(document.createElement('div'));
        pauseElement.addClass("fa");
        pauseElement.addClass("fa-pause");
        pauseElement.addClass("activity-pause-button");
        pauseElement.addClass("activity-ends-buttons");
        pauseElement.text(" pause");
        // pauseElement.on('click', activity_pauseButton_click(activityBox, timer));

        var stopElement = $(document.createElement('div'));
        stopElement.addClass("fa");
        stopElement.addClass("fa-stop");
        stopElement.addClass("activity-stop-button");
        stopElement.addClass("activity-ends-buttons");
        stopElement.text(" stop");
        //stopElement.on('click', activity_endButton_click(activityBox, timer));

        var documentIdElement = $(document.createElement('div'));
        documentIdElement.addClass("activity-fileId");
        documentIdElement.addClass("activity-information-right");

        var documentIdElementHead = $(document.createElement('span'));
        documentIdElementHead.text("Smlouva: ");
        var documentIdElementValue = $(document.createElement('span'));
        documentIdElementValue.text($(".active-activity-document").text());
        documentIdElement.append(documentIdElementHead);
        documentIdElement.append(documentIdElementValue);
        var customerIdElement = $(document.createElement('div'));
        customerIdElement.addClass("activity-customerId");
        customerIdElement.addClass("activity-information-right");
        var customerIdElementHead = $(document.createElement('span'));
        customerIdElementHead.text("Klient: ");
        var customerIdElementValue = $(document.createElement('span'));
        customerIdElementValue.text($(".active-activity-customer").text());

        customerIdElement.append(customerIdElementHead);
        customerIdElement.append(customerIdElementValue);

        activityBox.find("div.text-right").append(customerIdElement);
        activityBox.find("div.text-right").append(documentIdElement);

        activityBox.find(".fa-arrow-circle-right").remove();

        activityBox.find("span.pull-right").append(pauseElement);

        activityBox.find("span.pull-right").append(stopElement);


        $(".activity-pause-button").click(function () {
            console.log("pause")
            var pauseButton = $('#modal-activity-pause').find("#pause-activity-button");
            $('#modal-activity-pause').find('h3.modal-header').text($(".active-activity-name").text());
            $('#modal-activity-pause').find('span.modal-created').text("Vytvořeno: " + $(".active-activity-created").text());
            $('#modal-activity-pause').find('span.modal-updated').text("aktualizováno: " + $(".active-activity-updated").text());
            $('#modal-activity-pause').find('#activity-pause-client-id').val($(".active-activity-customer").text());
            $('#modal-activity-pause').find('#activity-pause-file-id').val($(".active-activity-document").text());
            $('#modal-activity-pause').find('#activity-pause-comment').val($(".active-activity-comment").text());
            $('#modal-activity-pause').find('#pause-activity-button').on('click', pauseButton, pauseButtonClick($(this)));
            $('#modal-activity-pause').modal({backdrop: 'static', keyboard: false});
            console.log("pause")

        });


        $(".activity-stop-button").click(function () {


            var stopButton = $('#modal-activity-stop').find("#stop-activity-button");
            $('#modal-activity-stop').find('h3.modal-header').text($(".active-activity-name").text());
            $('#modal-activity-stop').find('span.modal-created').text("Vytvořeno: " + $(".active-activity-created").text());
            $('#modal-activity-stop').find('span.modal-updated').text("aktualizováno: " + $(".active-activity-updated").text());
            $('#modal-activity-stop').find('#activity-stop-client-id').val($(".active-activity-customer").text());
            $('#modal-activity-stop').find('#activity-stop-file-id').val($(".active-activity-document").text());
            $('#modal-activity-stop').find('#activity-stop-comment').val($(".active-activity-comment").text());
            $('#modal-activity-stop').find('#stop-activity-button').on('click', stopButton, stopButtonClick());
            $('#modal-activity-stop').modal({backdrop: 'static', keyboard: false});


        });


    }


    var bg_initial = $('#page-wrapper').find('.nav-item.active').children().first().css("background-color");

    $('.panel-activity').css({'background-color': bg_initial});
    $('.panel-primary').css({'border-color': bg_initial});
    $('.activity-tab-line').css({'border-color': bg_initial});


    $('.nav-link').on("click", function () {
        var bg = $(this).css("background-color");

        $('.panel-activity').css({'background-color': bg});
        $('.panel-primary').css({'border-color': bg});
        $('.activity-tab-line').css({'border-color': bg});

    });


    (function ($) {
        $.fn.fa = function (options) {
            options = $.extend({
                icon: "home",
                where: "before"
            }, options);

            return this.each(function () {
                var $element = $(this);
                var icon = "<i class='fa fa-" + options.icon + "'></i>";
                $element[options.where](icon);
            });
        };
    })(jQuery);

    //listener for click to activity panel
    $(document).on("click", '.panel-primary', function () {
        var timer = new Timer();
        timer.start();
        if ((($("div.activity-timer").length == 0)) && ($(this).find(".fa-arrow-circle-right"))) {
            var name = $(this).find('span.pull-left').text();


            $('#modal-activity-start').find('h3.modal-header').text(name);
            $('#modal-activity-start').find('#client-id').val("");
            $('#modal-activity-start').find('#file-id').val("");

            var timerElement = $(document.createElement('div'));
            timerElement.addClass("activity-timer");
            timerElement.addClass("activity-information-right");


            timer.addEventListener('secondsUpdated', function (e) {


                timerElement.text(timer.getTimeValues().toString());
                var avgText = $('.panel-primary').find("span:contains('" + name + "')").filter(function () {
                    return $(this).text() === name;
                }).parent().parent().parent().find(".activity-avg").text();

                var avg = new Date("January 1, 2000 " + avgText);
                avg = avg.getTime();

                var actual = new Date("January 1, 2000 " + timer.getTimeValues());
                actual = actual.getTime();
                if ((localStorage.getItem('running-activity-id') != null) && (localStorage.getItem('running-template-id') != null)) {
                    localStorage.setItem('running-activity-timer', timer.getTimeValues().toString());
                }
                if (actual % 15000 == 0) {
                    updateActivityTime(localStorage.getItem('running-activity-timer'))
                }
                if (actual > avg) {

                    timerElement.css({'color': 'red'});
                } else {
                    timerElement.css({'color': 'white'});
                }

            });


            $(this).find(".text-right").append(timerElement);
            var startButton = $('#start-activity-button');

            var cancelButton = $('#modal-activity-start').find("#cancel-activity-button");
            cancelButton.off();
            $('#modal-activity-start').find('.modal-footer').append(startButton);
            $('#start-activity-button').on('click', startButton, modal_startButton_click($(this), timer));
            $('#cancel-activity-button').on('click', cancelButton, modal_cancelButton_click($(this), timer));
            //cancelButton.on('click',modal_cancelButton_click($(this),timer));

            localStorage.setItem('running-template-id', $('.panel-primary').find("span:contains('" + name + "')").filter(function () {
                return $(this).text() === name;
            }).parent().parent().parent().find(".activity-id").text());

            $(".radio-single-activity:first").attr('checked');
            $('#modal-activity-start').modal({backdrop: 'static', keyboard: false});
        }
    });

    //function for start activity in modal window
    function modal_startButton_click(param, timer) {
        return function () {
            var name = $(this).parent().parent().find('h3.modal-header').text();
            var pauseElement = $(document.createElement('div'));
            pauseElement.addClass("fa");
            pauseElement.addClass("fa-pause");
            pauseElement.addClass("activity-pause-button");
            pauseElement.addClass("activity-ends-buttons");
            pauseElement.text(" pause");


            var stopElement = $(document.createElement('div'));
            stopElement.addClass("fa");
            stopElement.addClass("fa-stop");
            stopElement.addClass("activity-stop-button");
            stopElement.addClass("activity-ends-buttons");
            stopElement.text(" stop");


            var documentIdElement = $(document.createElement('div'));
            documentIdElement.addClass("activity-fileId");
            documentIdElement.addClass("activity-information-right");

            var documentIdElementHead = $(document.createElement('span'));
            documentIdElementHead.text("Smlouva: ");
            var documentIdElementValue = $(document.createElement('span'));
            documentIdElementValue.text($(this).parents().find("#file-id").val());
            documentIdElement.append(documentIdElementHead);
            documentIdElement.append(documentIdElementValue);
            var customerIdElement = $(document.createElement('div'));
            customerIdElement.addClass("activity-customerId");
            customerIdElement.addClass("activity-information-right");
            var customerIdElementHead = $(document.createElement('span'));
            customerIdElementHead.text("Klient: ");
            var customerIdElementValue = $(document.createElement('span'));
            customerIdElementValue.text($(this).parents().find("#client-id").val());

            customerIdElement.append(customerIdElementHead);
            customerIdElement.append(customerIdElementValue);

            param.find("div.text-right").append(customerIdElement);
            param.find("div.text-right").append(documentIdElement);

            param.find(".fa-arrow-circle-right").remove();


            if ($("input[name=single-activity]:checked").val() == "yes") {
                if ((documentIdElementValue.text() == "") && (customerIdElementValue.text() == "")) {
                    alert("Vyplňte jedno z políček 'ID klienta' nebo 'Číslo smlouvy'");

                    return false;

                }
                var singleActivity = 1
            } else {
                var singleActivity = 0
            }

            if (localStorage.getItem("running-activity-timer") == undefined) {

                $.ajaxSetup({
                    headers: {"X-CSRFToken": csrftoken}
                });
                $.ajax({
                    type: "POST",
                    url: "/new_activity/",
                    async: true,
                    cache: false,
                    data: {
                        'post_id': $(this).attr('name'),
                        'document_id': documentIdElementValue.text(),
                        'customer_id': customerIdElementValue.text(),
                        'template_id': localStorage.getItem('running-template-id'),
                        'total_time': $('.panel-primary').find("span:contains('" + name + "')").filter(function () {
                            return $(this).text() === name;
                        }).parent().parent().parent().find(".activity-timer").text(),
                        'csrfmiddlewaretoken': csrftoken,
                        'activity_count': singleActivity,
                    },
                    success: function (response) {


                        if (response == "is_running") {
                            alert("Nelze vytvořit. Máte již běžící aktivitu");
                            location.reload();
                        } else {

                            $(document).trigger("add-alerts", [
                                {
                                    "message": "Aktivita  <b>'" + name + "'</b> úspěšně spuštěna.",
                                    "priority": 'success'
                                }
                            ]);
                            if (response != "unauthorized") {

                                localStorage.setItem("running-activity-id", response);
                                localStorage.setItem("running-activity-timer", $('.panel-primary').find("span:contains('" + name + "')").parent().parent().parent().find(".activity-timer").text());
                            }

                        }

                    },
                    error: function (rs, e) {

                        console.log("error " + rs.responseText);
                    }, complete: function () {

                        location.reload();
                    },
                });

            }
        }
    }

    //function for cancel activity in modal window
    function modal_cancelButton_click(param, timer) {
        return function () {
            var name = $(this).parent().parent().find('h3.modal-header').text();
            timer.stop();
            timer.removeEventListener();
            $("div.activity-timer").remove();
            localStorage.removeItem("running-template-id");
            localStorage.removeItem("running-template-");
            localStorage.removeItem("running-template-id");
            console.log("test cancel")

            $(document).trigger("add-alerts", [
                {
                    "message": "Spuštění aktivity <b>'" + name + "'</b> úspěšně zrušeno.",
                    "priority": 'warning'
                }]);
        }
    }

    $("#continue-cancel-button").click(function () {
        console.log("click cancel")
        $('#continue-activity-button').off();
        $('#modal-activity-continue').modal('hide');
    });
    

    $(".footer-item").click(function () {

        var continueButton = $('#modal-activity-start').find("#pause-activity-button");
        $('#modal-activity-continue').find('h3.modal-header').text("Pozastaveno: " + $(this).find(".footer-hidden-name").text());
        $('#modal-activity-continue').find('span.modal-created').text("Vytvořeno: " + $(this).find(".footer-hidden-created").text());
        $('#modal-activity-continue').find('span.modal-updated').text("aktualizováno: " + $(this).find(".footer-hidden-updated").text());
        $('#modal-activity-continue').find('#activity-continue-client-id').val($(this).find(".footer-hidden-client").text());
        $('#modal-activity-continue').find('#activity-continue-file-id').val($(this).find(".footer-hidden-file").text());
        $('#modal-activity-continue').find('#activity-continue-comment').val($(this).find(".footer-hidden-comment").text());
        $('#modal-activity-continue').find('#continue-activity-button').on('click', continueButton, continueButtonClick($(this)));
        $('#modal-activity-continue').modal({backdrop: 'static', keyboard: false});
        console.log("footer")
    });


    $(document).ready(function () {
        $('.dropdown-submenu a.test').on("click", function (e) {
            $(this).next('ul').toggle();
            e.stopPropagation();
            e.preventDefault();
        });
    });


    $('.module-selector').change(function () {


        $('#collection-date').datetimepicker({
            format: 'DD.MM.YYYY'
        });

        name = $('.module-selector').attr("name")
        if ($('.module-selector option:selected').attr("show_forms") == "True") {

            $("." + name + "-form").show();
        } else {
            $("." + name + "-form").hide();
        }
        ;
    });




});
