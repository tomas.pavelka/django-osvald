$(document).ready(function () {

if ($("#wrapper").html()!="") {
    updateResolution($(window).width(), $(window).height());
}


    $(window).bind('resize', function (e) {
        window.resizeEvt;
        $(window).resize(function () {
            clearTimeout(window.resizeEvt);
            window.resizeEvt = setTimeout(function () {
                updateResolution($(window).width(), $(window).height());
                location.reload()

            }, 300);
        });

    });

        function getCookie(name) {
            var cookieValue = null;
            if (document.cookie && document.cookie !== '') {
                var cookies = document.cookie.split(';');
                for (var i = 0; i < cookies.length; i++) {
                    var cookie = jQuery.trim(cookies[i]);
                    // Does this cookie string begin with the name we want?
                    if (cookie.substring(0, name.length + 1) === (name + '=')) {
                        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                        break;
                    }
                }
            }
            return cookieValue;
        }

        function updateResolution(x, y) {
        var csrftoken = getCookie('csrftoken');
            $.ajaxSetup({
                headers: {"X-CSRFToken": getCookie("csrftoken")}
            });
            $.ajax({
                type: "POST",
                url: "/update_resolution/",
                async: true,
                cache: true,
                data: {
                    'y': y,
                    'x': x,
                    'csrfmiddlewaretoken': csrftoken,
                },

            });


        }
});
