from functools import wraps
from django.conf import settings
from django.utils.decorators import available_attrs
from django.contrib.auth.decorators import login_required
from django.shortcuts import resolve_url
from osvald.models import SessionKey #my additional model


def unique_login_required(view_func):
    @wraps(view_func, assigned=available_attrs(view_func))
    def _wrapped_view(request, *args, **kwargs):
        r = False
        ...
        #check session key is equal to last one
        ...
        if r:
            return view_func(request, *args, **kwargs)
        else:
            from django.contrib.auth.views import redirect_to_login
            path = request.build_absolute_uri()
            resolved_login_url = resolve_url(settings.LOGIN_URL)
            return redirect_to_login(path,resolved_login_url)
    return _wrapped_view