
from .models import Activity
import django_filters
from django_filters import widgets,DateFromToRangeFilter


class DateFilter(django_filters.FilterSet):
    updated_at = DateFromToRangeFilter(label="Filtrovat v rozmezí",widget=widgets.RangeWidget(attrs={'placeholder': 'YYYY-MM-DD','type': 'date'}))
    class Meta:
        model = Activity
        fields = ['updated_at', ]