from django.http import HttpResponse
from django.shortcuts import redirect
from django.http import Http404, HttpResponseForbidden
from django.core.exceptions import PermissionDenied
from django.shortcuts import render
from .models import Category_activity
from .models import Template_activity
from .models import Logical_team
from .models import Activity, Group
from .forms import *
from .dashboard import *
from graphos.sources.simple import SimpleDataSource
from graphos.sources.model import ModelDataSource
from graphos.renderers.gchart import PieChart, LineChart, ColumnChart, ComboChart, CalendarChart
from django.views.decorators.csrf import csrf_exempt
from chartit import DataPool, Chart
from django.contrib.auth.decorators import login_required, user_passes_test
from datetime import datetime
from .osvald_views import *
from random import randint
from datetime import timedelta
from django.utils import timezone
from random import randrange
from django.views.generic import ListView, CreateView, UpdateView
import datetime
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django_tables2 import RequestConfig
from .tables import CollectionsTable,DetailTable,DetailKapacityTable
from django_filters.views import FilterView
from django_tables2.views import SingleTableMixin
from django_tables2.export.views import ExportMixin
from django_tables2.export.export import TableExport
from .filters import DateFilter
from dateutil.relativedelta import relativedelta


import random


def managers(request):
    if is_manager(request.user):
        return {"superior": superior_groups(request), "dependent": dependent_groups(request),
                "dependent_users": dependent_users(request), }
    else:
        return False



# =====> template
def user_all_dependend_users_view(request, username):
    manager = managers(request)

    dependent_users = []
    for u in manager["dependent_users"]:
        dependent_users.append(u.id)

    try:
        u = User.objects.get(username=username)
    except:
        raise Http404("Uživatel neexistuje")

    now = datetime.datetime.now().strftime('%d.%m.%Y')
    prehled_aktivit = graf_prehled_aktivit_group(request, "Všichni podřízení uživatele " + u.username,Activity.objects.raw(users_prehled_kapacit_sql(request,dependent_users)),now)
    mesicni_prehled_dokoncenych_aktivit = graf_mesicni_prehled_dokoncenych_aktivit(request,"Všichni podřízení uživatele " + u.username,Activity.objects.raw(users_mesicni_prehled_dokoncenych_sql(request,dependent_users)) ,now)
    mesicni_prehled_dokoncenych_aktivit_top_pocet = graf_mesicni_prehled_dokoncenych_aktivit_top_pocet(request,"Všichni podřízení uživatele " +  u.username,Activity.objects.raw(users_top_pocet_sql(request,dependent_users)),now)
    mesicni_prehled_dokoncenych_aktivit_top_cas = graf_mesicni_prehled_dokoncenych_aktivit_top_cas(request,"Všichni podřízení uživatele " +  u.username,Activity.objects.raw(users_top_cas_sql(request,dependent_users)),now)
    srovnani_zpracovanych_pozadavku = graf_srovnani_zpracovanych_pozadavku_po_dnech(request, "Všichni podřízení uživatele " +  u.username, Activity.objects.raw(users_srovnani_zpracovanych_pozadavku_sql(request,dependent_users)), now)
    mesicni_prehled_kapacit = graf_mesicni_prehled_kapacit(request,"Všichni podřízení uživatele " +  u.username,Activity.objects.raw(users_mesicni_prehled_kapacit_sql(request,dependent_users)),now)
    mesicni_prehled_efektivni = graf_kapacity_efektivni_cas(request,"Všichni podřízení uživatele " +  u.username,Activity.objects.raw(users_mesicni_prehled_efektivni_sql(request,dependent_users)),now)
    rozpad = graf_rozpad_pracovni_doby(request, u.username,Activity.objects.raw(user_dependent_prehled_rozpad_doby_sql(request,dependent_users)),now)
    mesicni_prehled_ztrata = graf_ztrata_efektivity(request,"Všichni podřízení uživatele " +  u.username,Activity.objects.raw(users_mesicni_prehled_ztrata_sql(request,dependent_users)),now)
    rizeni_vykonnosti = graf_rizeni_vykonnosti(request,"Všichni podřízení uživatele " +  u.username,Activity.objects.raw(users_dependent_rizeni_vykonnosti_sql(request,dependent_users)),now)
    rozpad_detail = graf_rozpad_pracovni_doby(request, u.username, Activity.objects.raw(user_dependent_prehled_rozpad_doby_sql_detail(request, dependent_users)), now)

    graph = {
        'prehled_aktivit':prehled_aktivit,
        'mesicni_prehled_dokoncenych_aktivit_top_pocet': mesicni_prehled_dokoncenych_aktivit_top_pocet,
        'mesicni_prehled_dokoncenych_aktivit_top_cas': mesicni_prehled_dokoncenych_aktivit_top_cas,
        'mesicni_prehled_dokoncenych_aktivit': mesicni_prehled_dokoncenych_aktivit,
        'srovnani_zpracovanych_pozadavku': srovnani_zpracovanych_pozadavku,
        'mesicni_prehled_kapacit':mesicni_prehled_kapacit,
        'kapacity_efektivni_cas': mesicni_prehled_efektivni,
        'ztrata_efektivity': mesicni_prehled_ztrata,
        'rizeni_vykonnosti':rizeni_vykonnosti,
        'rozpad_pracovni_doby':rozpad,
        'rozpad_pracovni_doby_detail':rozpad_detail,
    }

    return render(request, "osvald/user_all_dependend_users_view.html",
                  {"manager": manager, 'user_info': u, 'graf':graph ,'users': dependent_users,})


def user_all_dependend_logic_view(request, username):
    manager = managers(request)

    dependent_users = []
    for u in manager["dependent_users"]:
        dependent_users.append(u.id)

    try:
        u = User.objects.get(username=username)
    except:
        raise Http404("Uživatel neexistuje")

    now = datetime.datetime.now().strftime('%d.%m.%Y')
    prehled_aktivit = graf_prehled_aktivit_group(request, "Všichni podřízení uživatele " + u.username,
                                                 Activity.objects.raw(
                                                     users_prehled_kapacit_sql(request, dependent_users)), now)
    mesicni_prehled_dokoncenych_aktivit = graf_mesicni_prehled_dokoncenych_aktivit(request,
                                                                                   "Všichni podřízení uživatele " + u.username,
                                                                                   Activity.objects.raw(
                                                                                       users_mesicni_prehled_dokoncenych_sql(
                                                                                           request, dependent_users)),
                                                                                   now)
    mesicni_prehled_dokoncenych_aktivit_top_pocet = graf_mesicni_prehled_dokoncenych_aktivit_top_pocet(request,
                                                                                                       "Všichni podřízení uživatele " + u.username,
                                                                                                       Activity.objects.raw(
                                                                                                           users_top_pocet_sql(
                                                                                                               request,
                                                                                                               dependent_users)),
                                                                                                       now)
    mesicni_prehled_dokoncenych_aktivit_top_cas = graf_mesicni_prehled_dokoncenych_aktivit_top_cas(request,
                                                                                                   "Všichni podřízení uživatele " + u.username,
                                                                                                   Activity.objects.raw(
                                                                                                       users_top_cas_sql(
                                                                                                           request,
                                                                                                           dependent_users)),
                                                                                                   now)
    srovnani_zpracovanych_pozadavku = graf_srovnani_zpracovanych_pozadavku_po_dnech(request,
                                                                                    "Všichni podřízení uživatele " + u.username,
                                                                                    Activity.objects.raw(
                                                                                        users_srovnani_zpracovanych_pozadavku_sql(
                                                                                            request, dependent_users)),
                                                                                    now)
    mesicni_prehled_kapacit = graf_mesicni_prehled_kapacit(request, "Všichni podřízení uživatele " + u.username,
                                                           Activity.objects.raw(
                                                               users_mesicni_prehled_kapacit_sql(request,
                                                                                                 dependent_users)), now)
    mesicni_prehled_efektivni = graf_kapacity_efektivni_cas(request, "Všichni podřízení uživatele " + u.username,
                                                            Activity.objects.raw(
                                                                users_mesicni_prehled_efektivni_sql(request,
                                                                                                    dependent_users)),
                                                            now)
    rozpad = graf_rozpad_pracovni_doby(request, u.username, Activity.objects.raw(
        user_dependent_prehled_rozpad_doby_sql(request, dependent_users)), now)
    rozpad_detail = graf_rozpad_pracovni_doby(request, u.username, Activity.objects.raw(
        user_dependent_prehled_rozpad_doby_sql_detail(request, dependent_users)), now)
    mesicni_prehled_ztrata = graf_ztrata_efektivity(request, "Všichni podřízení uživatele " + u.username,
                                                    Activity.objects.raw(
                                                        users_mesicni_prehled_ztrata_sql(request, dependent_users)),
                                                    now)
    rizeni_vykonnosti = graf_rizeni_vykonnosti(request, "Všichni podřízení uživatele " + u.username,
                                               Activity.objects.raw(
                                                   users_dependent_rizeni_vykonnosti_sql(request, dependent_users)),
                                               now)

    graph = {
        'prehled_aktivit': prehled_aktivit,
        'mesicni_prehled_dokoncenych_aktivit_top_pocet': mesicni_prehled_dokoncenych_aktivit_top_pocet,
        'mesicni_prehled_dokoncenych_aktivit_top_cas': mesicni_prehled_dokoncenych_aktivit_top_cas,
        'mesicni_prehled_dokoncenych_aktivit': mesicni_prehled_dokoncenych_aktivit,
        'srovnani_zpracovanych_pozadavku': srovnani_zpracovanych_pozadavku,
        'mesicni_prehled_kapacit': mesicni_prehled_kapacit,
        'kapacity_efektivni_cas': mesicni_prehled_efektivni,
        'ztrata_efektivity': mesicni_prehled_ztrata,
        'rizeni_vykonnosti': rizeni_vykonnosti,
        'rozpad_pracovni_doby': rozpad,
        'rozpad_pracovni_doby_detail': rozpad_detail,
    }

    return render(request, "osvald/user_all_dependend_logic_view.html",
                  {"manager": manager, 'user_info': u,'graf':graph ,'users': dependent_users })


def user_all_dependend_group_view(request, username):
    manager = managers(request)

    dependent_users = []
    for u in manager["dependent_users"]:
        dependent_users.append(u.id)

    try:
        u = User.objects.get(username=username)
    except:
        raise Http404("Uživatel neexistuje")

    now = datetime.datetime.now().strftime('%d.%m.%Y')
    prehled_aktivit = graf_prehled_aktivit_group(request, "Všichni podřízení uživatele " + u.username,
                                                 Activity.objects.raw(
                                                     users_prehled_kapacit_sql(request, dependent_users)), now)
    mesicni_prehled_dokoncenych_aktivit = graf_mesicni_prehled_dokoncenych_aktivit(request,
                                                                                   "Všichni podřízení uživatele " + u.username,
                                                                                   Activity.objects.raw(
                                                                                       users_mesicni_prehled_dokoncenych_sql(
                                                                                           request, dependent_users)),
                                                                                   now)
    mesicni_prehled_dokoncenych_aktivit_top_pocet = graf_mesicni_prehled_dokoncenych_aktivit_top_pocet(request,
                                                                                                       "Všichni podřízení uživatele " + u.username,
                                                                                                       Activity.objects.raw(
                                                                                                           users_top_pocet_sql(
                                                                                                               request,
                                                                                                               dependent_users)),
                                                                                                       now)
    mesicni_prehled_dokoncenych_aktivit_top_cas = graf_mesicni_prehled_dokoncenych_aktivit_top_cas(request,
                                                                                                   "Všichni podřízení uživatele " + u.username,
                                                                                                   Activity.objects.raw(
                                                                                                       users_top_cas_sql(
                                                                                                           request,
                                                                                                           dependent_users)),
                                                                                                   now)
    srovnani_zpracovanych_pozadavku = graf_srovnani_zpracovanych_pozadavku_po_dnech(request,
                                                                                    "Všichni podřízení uživatele " + u.username,
                                                                                    Activity.objects.raw(
                                                                                        users_srovnani_zpracovanych_pozadavku_sql(
                                                                                            request, dependent_users)),
                                                                                    now)
    mesicni_prehled_kapacit = graf_mesicni_prehled_kapacit(request, "Všichni podřízení uživatele " + u.username,
                                                           Activity.objects.raw(
                                                               users_mesicni_prehled_kapacit_sql(request,
                                                                                                 dependent_users)), now)
    mesicni_prehled_efektivni = graf_kapacity_efektivni_cas(request, "Všichni podřízení uživatele " + u.username,
                                                            Activity.objects.raw(
                                                                users_mesicni_prehled_efektivni_sql(request,
                                                                                                    dependent_users)),
                                                            now)
    rozpad = graf_rozpad_pracovni_doby(request, u.username, Activity.objects.raw(
        user_dependent_prehled_rozpad_doby_sql(request, dependent_users)), now)
    rozpad_detail = graf_rozpad_pracovni_doby(request, u.username, Activity.objects.raw(
        user_dependent_prehled_rozpad_doby_sql_detail(request, dependent_users)), now)
    mesicni_prehled_ztrata = graf_ztrata_efektivity(request, "Všichni podřízení uživatele " + u.username,
                                                    Activity.objects.raw(
                                                        users_mesicni_prehled_ztrata_sql(request, dependent_users)),
                                                    now)
    rizeni_vykonnosti = graf_rizeni_vykonnosti(request, "Všichni podřízení uživatele " + u.username,
                                               Activity.objects.raw(
                                                   users_dependent_rizeni_vykonnosti_sql(request, dependent_users)),
                                               now)

    graph = {
        'prehled_aktivit': prehled_aktivit,
        'mesicni_prehled_dokoncenych_aktivit_top_pocet': mesicni_prehled_dokoncenych_aktivit_top_pocet,
        'mesicni_prehled_dokoncenych_aktivit_top_cas': mesicni_prehled_dokoncenych_aktivit_top_cas,
        'mesicni_prehled_dokoncenych_aktivit': mesicni_prehled_dokoncenych_aktivit,
        'srovnani_zpracovanych_pozadavku': srovnani_zpracovanych_pozadavku,
        'mesicni_prehled_kapacit': mesicni_prehled_kapacit,
        'kapacity_efektivni_cas': mesicni_prehled_efektivni,
        'ztrata_efektivity': mesicni_prehled_ztrata,
        'rizeni_vykonnosti': rizeni_vykonnosti,
        'rozpad_pracovni_doby': rozpad,
        'rozpad_pracovni_doby_detail': rozpad_detail,
    }

    return render(request, "osvald/user_all_dependend_group_view.html",
                  {"manager": manager, 'user_info': u,'graf':graph ,'users': dependent_users })


def group_activity_view(request, group):
    manager = managers(request)
    id_group = Group.objects.get(name=group).id

    info = { "id":id_group,"name":group}

    dependent_users = []
    for g in Group.objects.get(name=group).get_descendants(include_self=True).all():
        for u in g.user.all():
            dependent_users.append(u.id)


    now = datetime.datetime.now().strftime('%d.%m.%Y')
    srovnani_zpracovanych_pozadavku = graf_srovnani_zpracovanych_pozadavku_po_dnech(request, group, Activity.objects.raw(group_srovnani_zpracovanych_pozadavku_sql(request,id_group)), now)
    mesicni_prehled_dokoncenych_aktivit = graf_mesicni_prehled_dokoncenych_aktivit(request, group,Activity.objects.raw(group_mesicni_prehled_dokoncenych_sql(request,id_group)) ,now)
    mesicni_prehled_dokoncenych_aktivit_top_pocet = graf_mesicni_prehled_dokoncenych_aktivit_top_pocet(request, group,Activity.objects.raw(group_top_pocet_sql(request,id_group)),now)
    mesicni_prehled_dokoncenych_aktivit_top_cas = graf_mesicni_prehled_dokoncenych_aktivit_top_cas(request, group,Activity.objects.raw(group_top_cas_sql(request,id_group)),now)
    prehled_aktivit = graf_prehled_aktivit_group(request, group,Activity.objects.raw(group_prehled_kapacit_sql(request,id_group)),now)
    mesicni_prehled_kapacit = graf_mesicni_prehled_kapacit(request, group,Activity.objects.raw(group_mesicni_prehled_kapacit_sql(request,id_group)),now)
    mesicni_prehled_efektivni = graf_kapacity_efektivni_cas(request, group,Activity.objects.raw(group_mesicni_prehled_efektivni_sql(request,id_group)),now)
    rozpad = graf_rozpad_pracovni_doby(request, group,Activity.objects.raw(group_mesicni_prehled_rozpad_doby_sql(request,id_group)),now)
    mesicni_prehled_ztrata = graf_ztrata_efektivity(request, group,Activity.objects.raw(group_mesicni_prehled_ztrata_sql(request,id_group)),now)
    rizeni_vykonnosti = graf_rizeni_vykonnosti(request, group,Activity.objects.raw(group_rizeni_vykonnosti_sql(request,id_group)),now)
    rozpad_detail = graf_rozpad_pracovni_doby(request, group,Activity.objects.raw(group_mesicni_prehled_rozpad_doby_sql_detail(request, id_group)),now)

    graph = {
        'prehled_aktivit':prehled_aktivit,
        'mesicni_prehled_dokoncenych_aktivit_top_pocet': mesicni_prehled_dokoncenych_aktivit_top_pocet,
        'mesicni_prehled_dokoncenych_aktivit_top_cas': mesicni_prehled_dokoncenych_aktivit_top_cas,
        'mesicni_prehled_dokoncenych_aktivit': mesicni_prehled_dokoncenych_aktivit,
        'srovnani_zpracovanych_pozadavku': srovnani_zpracovanych_pozadavku,
        'mesicni_prehled_kapacit':mesicni_prehled_kapacit,
        'kapacity_efektivni_cas': mesicni_prehled_efektivni,
        'ztrata_efektivity': mesicni_prehled_ztrata,
        'rizeni_vykonnosti':rizeni_vykonnosti,
        'rozpad_pracovni_doby':rozpad,
        'rozpad_pracovni_doby_detail':rozpad_detail,
    }

    return render(request, "osvald/group_activity_view.html",
                  {"manager": manager, 'info': info, 'graf':graph ,'users': dependent_users})


def group_all_dependend_group_view(request, group):

    try:
     g = Group.objects.get(name=group)
    except:
        raise Http404("Skupina neexistuje")

    dependent_users = []
    for g in Group.objects.get(name=group).get_descendants(include_self=True).all():
        for u in g.user.all():
            dependent_users.append(u.id)
    manager = managers(request)


    now = datetime.datetime.now().strftime('%d.%m.%Y')
    prehled_aktivit = graf_prehled_aktivit_group(request, "Všichni podřízení členové týmu " + g.name,
                                                 Activity.objects.raw(
                                                     users_prehled_kapacit_sql(request, dependent_users)), now)
    mesicni_prehled_dokoncenych_aktivit = graf_mesicni_prehled_dokoncenych_aktivit(request,
                                                                                   "Všichni podřízení členové týmu " + g.name,
                                                                                   Activity.objects.raw(
                                                                                       users_mesicni_prehled_dokoncenych_sql(
                                                                                           request, dependent_users)),
                                                                                   now)
    mesicni_prehled_dokoncenych_aktivit_top_pocet = graf_mesicni_prehled_dokoncenych_aktivit_top_pocet(request,
                                                                                                       "Všichni podřízení členové týmu " + g.name,
                                                                                                       Activity.objects.raw(
                                                                                                           users_top_pocet_sql(
                                                                                                               request,
                                                                                                               dependent_users)),
                                                                                                       now)
    mesicni_prehled_dokoncenych_aktivit_top_cas = graf_mesicni_prehled_dokoncenych_aktivit_top_cas(request,
                                                                                                   "Všichni podřízení členové týmu " + g.name,
                                                                                                   Activity.objects.raw(
                                                                                                       users_top_cas_sql(
                                                                                                           request,
                                                                                                           dependent_users)),
                                                                                                   now)
    srovnani_zpracovanych_pozadavku = graf_srovnani_zpracovanych_pozadavku_po_dnech(request,
                                                                                    "Všichni podřízení členové týmu " + g.name,
                                                                                    Activity.objects.raw(
                                                                                        users_srovnani_zpracovanych_pozadavku_sql(
                                                                                            request, dependent_users)),
                                                                                    now)
    mesicni_prehled_kapacit = graf_mesicni_prehled_kapacit(request, "Všichni podřízení členové týmu " + g.name,
                                                           Activity.objects.raw(
                                                               users_mesicni_prehled_kapacit_sql(request,
                                                                                                 dependent_users)), now)
    mesicni_prehled_efektivni = graf_kapacity_efektivni_cas(request, "Všichni podřízení členové týmu " + g.name,
                                                            Activity.objects.raw(
                                                                users_mesicni_prehled_efektivni_sql(request,
                                                                                                    dependent_users)),
                                                            now)
    rozpad = graf_rozpad_pracovni_doby(request, "Všichni podřízení členové týmu " + g.name, Activity.objects.raw(
        user_dependent_prehled_rozpad_doby_sql(request, dependent_users)), now)
    rozpad_detail = graf_rozpad_pracovni_doby(request, "Všichni podřízení členové týmu " + g.name, Activity.objects.raw(
        user_dependent_prehled_rozpad_doby_sql_detail(request, dependent_users)), now)
    mesicni_prehled_ztrata = graf_ztrata_efektivity(request, "Všichni členové týmu " + g.name,
                                                    Activity.objects.raw(
                                                        users_mesicni_prehled_ztrata_sql(request, dependent_users)),
                                                    now)
    rizeni_vykonnosti = graf_rizeni_vykonnosti(request, "Všichni podřízení členové týmu " + g.name,
                                               Activity.objects.raw(
                                                   users_dependent_rizeni_vykonnosti_sql(request, dependent_users)),
                                               now)

    graph = {
        'prehled_aktivit': prehled_aktivit,
        'mesicni_prehled_dokoncenych_aktivit_top_pocet': mesicni_prehled_dokoncenych_aktivit_top_pocet,
        'mesicni_prehled_dokoncenych_aktivit_top_cas': mesicni_prehled_dokoncenych_aktivit_top_cas,
        'mesicni_prehled_dokoncenych_aktivit': mesicni_prehled_dokoncenych_aktivit,
        'srovnani_zpracovanych_pozadavku': srovnani_zpracovanych_pozadavku,
        'mesicni_prehled_kapacit': mesicni_prehled_kapacit,
        'kapacity_efektivni_cas': mesicni_prehled_efektivni,
        'ztrata_efektivity': mesicni_prehled_ztrata,
        'rizeni_vykonnosti': rizeni_vykonnosti,
        'rozpad_pracovni_doby': rozpad,
        'rozpad_pracovni_doby_detail': rozpad_detail,
    }


    return render(request, "osvald/group_all_dependend_group_view.html",
                  {"manager": manager, 'user_info': group,'graf':graph ,'users': dependent_users })


def group_all_dependend_logic_view(request, group):

    try:
     g = Group.objects.get(name=group)
    except:
        raise Http404("Skupina neexistuje")

    dependent_users = []
    for l in Group.objects.get(name=group).logical_team.all():
        for u in l.user.all():
            dependent_users.append(u.id)
    manager = managers(request)


    now = datetime.datetime.now().strftime('%d.%m.%Y')
    prehled_aktivit = graf_prehled_aktivit_group(request, "Všichni členové logických týmů v týmu " + g.name,
                                                 Activity.objects.raw(
                                                     users_prehled_kapacit_sql(request, dependent_users)), now)
    mesicni_prehled_dokoncenych_aktivit = graf_mesicni_prehled_dokoncenych_aktivit(request,
                                                                                   "Všichni členové logických týmů v týmu " + g.name,
                                                                                   Activity.objects.raw(
                                                                                       users_mesicni_prehled_dokoncenych_sql(
                                                                                           request, dependent_users)),
                                                                                   now)
    mesicni_prehled_dokoncenych_aktivit_top_pocet = graf_mesicni_prehled_dokoncenych_aktivit_top_pocet(request,
                                                                                                       "Všichni členové logických týmů v týmu " + g.name,
                                                                                                       Activity.objects.raw(
                                                                                                           users_top_pocet_sql(
                                                                                                               request,
                                                                                                               dependent_users)),
                                                                                                       now)
    mesicni_prehled_dokoncenych_aktivit_top_cas = graf_mesicni_prehled_dokoncenych_aktivit_top_cas(request,
                                                                                                   "Všichni členové logických týmů v týmu " + g.name,
                                                                                                   Activity.objects.raw(
                                                                                                       users_top_cas_sql(
                                                                                                           request,
                                                                                                           dependent_users)),
                                                                                                   now)
    srovnani_zpracovanych_pozadavku = graf_srovnani_zpracovanych_pozadavku_po_dnech(request,
                                                                                    "Všichni členové logických týmů v týmu " + g.name,
                                                                                    Activity.objects.raw(
                                                                                        users_srovnani_zpracovanych_pozadavku_sql(
                                                                                            request, dependent_users)),
                                                                                    now)
    mesicni_prehled_kapacit = graf_mesicni_prehled_kapacit(request, "Všichni členové logických týmů v týmu " + g.name,
                                                           Activity.objects.raw(
                                                               users_mesicni_prehled_kapacit_sql(request,
                                                                                                 dependent_users)), now)
    mesicni_prehled_efektivni = graf_kapacity_efektivni_cas(request, "Všichni členové logických týmů v týmu " + g.name,
                                                            Activity.objects.raw(
                                                                users_mesicni_prehled_efektivni_sql(request,
                                                                                                    dependent_users)),
                                                            now)
    rozpad = graf_rozpad_pracovni_doby(request, "Všichni členové logických týmů v týmu " + g.name, Activity.objects.raw(
        user_dependent_prehled_rozpad_doby_sql(request, dependent_users)), now)
    rozpad_detail = graf_rozpad_pracovni_doby(request, "Všichni členové logických týmů v týmu " + g.name, Activity.objects.raw(
        user_dependent_prehled_rozpad_doby_sql_detail(request, dependent_users)), now)
    mesicni_prehled_ztrata = graf_ztrata_efektivity(request, "Všichni členové logických týmů v týmu " + g.name,
                                                    Activity.objects.raw(
                                                        users_mesicni_prehled_ztrata_sql(request, dependent_users)),
                                                    now)
    rizeni_vykonnosti = graf_rizeni_vykonnosti(request, "Všichni členové logických týmů v týmu " + g.name,
                                               Activity.objects.raw(
                                                   users_dependent_rizeni_vykonnosti_sql(request, dependent_users)),
                                               now)

    graph = {
        'prehled_aktivit': prehled_aktivit,
        'mesicni_prehled_dokoncenych_aktivit_top_pocet': mesicni_prehled_dokoncenych_aktivit_top_pocet,
        'mesicni_prehled_dokoncenych_aktivit_top_cas': mesicni_prehled_dokoncenych_aktivit_top_cas,
        'mesicni_prehled_dokoncenych_aktivit': mesicni_prehled_dokoncenych_aktivit,
        'srovnani_zpracovanych_pozadavku': srovnani_zpracovanych_pozadavku,
        'mesicni_prehled_kapacit': mesicni_prehled_kapacit,
        'kapacity_efektivni_cas': mesicni_prehled_efektivni,
        'ztrata_efektivity': mesicni_prehled_ztrata,
        'rizeni_vykonnosti': rizeni_vykonnosti,
        'rozpad_pracovni_doby': rozpad,
        'rozpad_pracovni_doby_detail': rozpad_detail,
    }



    return render(request, "osvald/group_all_dependend_logic_view.html",
                  {"manager": manager, 'user_info': group,'graf':graph ,'users': dependent_users })




def logical_team_activity_view(request, name):
    dependent_users = []
    for u in Logical_team.objects.get(name=name).user.all():
        dependent_users.append(u.id)

    manager = managers(request)

    try:
        l = Logical_team.objects.get(name=name)
    except:
        raise Http404("Logický tým neexistuje")

    now = datetime.datetime.now().strftime('%d.%m.%Y')
    prehled_aktivit = graf_prehled_aktivit_group(request, "Všichni členové týmu " + l.name,
                                                 Activity.objects.raw(
                                                     users_prehled_kapacit_sql(request, dependent_users)), now)
    mesicni_prehled_dokoncenych_aktivit = graf_mesicni_prehled_dokoncenych_aktivit(request,
                                                                                   "Všichni členové týmu " + l.name,
                                                                                   Activity.objects.raw(
                                                                                       users_mesicni_prehled_dokoncenych_sql(
                                                                                           request, dependent_users)),
                                                                                   now)
    mesicni_prehled_dokoncenych_aktivit_top_pocet = graf_mesicni_prehled_dokoncenych_aktivit_top_pocet(request,
                                                                                                       "Všichni členové týmu " + l.name,
                                                                                                       Activity.objects.raw(
                                                                                                           users_top_pocet_sql(
                                                                                                               request,
                                                                                                               dependent_users)),
                                                                                                       now)
    mesicni_prehled_dokoncenych_aktivit_top_cas = graf_mesicni_prehled_dokoncenych_aktivit_top_cas(request,
                                                                                                   "Všichni členové týmu " + l.name,
                                                                                                   Activity.objects.raw(
                                                                                                       users_top_cas_sql(
                                                                                                           request,
                                                                                                           dependent_users)),
                                                                                                   now)
    srovnani_zpracovanych_pozadavku = graf_srovnani_zpracovanych_pozadavku_po_dnech(request,
                                                                                    "Všichni členové týmu " + l.name,
                                                                                    Activity.objects.raw(
                                                                                        users_srovnani_zpracovanych_pozadavku_sql(
                                                                                            request, dependent_users)),
                                                                                    now)
    mesicni_prehled_kapacit = graf_mesicni_prehled_kapacit(request, "Všichni členové týmu " + l.name,
                                                           Activity.objects.raw(
                                                               users_mesicni_prehled_kapacit_sql(request,
                                                                                                 dependent_users)), now)
    mesicni_prehled_efektivni = graf_kapacity_efektivni_cas(request, "Všichni členové týmu " + l.name,
                                                            Activity.objects.raw(
                                                                users_mesicni_prehled_efektivni_sql(request,
                                                                                                    dependent_users)),
                                                            now)
    rozpad = graf_rozpad_pracovni_doby(request, "Všichni členové týmu " + l.name, Activity.objects.raw(
        user_dependent_prehled_rozpad_doby_sql(request, dependent_users)), now)
    rozpad_detail = graf_rozpad_pracovni_doby(request, "Všichni členové týmu " + l.name, Activity.objects.raw(
        user_dependent_prehled_rozpad_doby_sql_detail(request, dependent_users)), now)
    mesicni_prehled_ztrata = graf_ztrata_efektivity(request, "Všichni členové týmu " + l.name,
                                                    Activity.objects.raw(
                                                        users_mesicni_prehled_ztrata_sql(request, dependent_users)),
                                                    now)
    rizeni_vykonnosti = graf_rizeni_vykonnosti(request, "Všichni členové týmu " + l.name,
                                               Activity.objects.raw(
                                                   users_dependent_rizeni_vykonnosti_sql(request, dependent_users)),
                                               now)

    graph = {
        'prehled_aktivit': prehled_aktivit,
        'mesicni_prehled_dokoncenych_aktivit_top_pocet': mesicni_prehled_dokoncenych_aktivit_top_pocet,
        'mesicni_prehled_dokoncenych_aktivit_top_cas': mesicni_prehled_dokoncenych_aktivit_top_cas,
        'mesicni_prehled_dokoncenych_aktivit': mesicni_prehled_dokoncenych_aktivit,
        'srovnani_zpracovanych_pozadavku': srovnani_zpracovanych_pozadavku,
        'mesicni_prehled_kapacit': mesicni_prehled_kapacit,
        'kapacity_efektivni_cas': mesicni_prehled_efektivni,
        'ztrata_efektivity': mesicni_prehled_ztrata,
        'rizeni_vykonnosti': rizeni_vykonnosti,
        'rozpad_pracovni_doby': rozpad,
        'rozpad_pracovni_doby_detail': rozpad_detail,
    }

    return render(request, "osvald/logical_team_activity_view.html",
                  {"manager": manager, 'user_info': name,'graf':graph ,'users': dependent_users })


def group_all_dependend_users_view(request, group):
    try:
        g = Group.objects.get(name=group)
    except:
        raise Http404("Skupina neexistuje")

    dependent_users = []
    for u in Group.objects.get(name=group).user.all():
        dependent_users.append(u.id)

    manager = managers(request)


    now = datetime.datetime.now().strftime('%d.%m.%Y')
    prehled_aktivit = graf_prehled_aktivit_group(request, "Všichni členové týmu " + g.name,
                                                 Activity.objects.raw(
                                                     users_prehled_kapacit_sql(request, dependent_users)), now)
    mesicni_prehled_dokoncenych_aktivit = graf_mesicni_prehled_dokoncenych_aktivit(request,
                                                                                   "Všichni členové týmu " + g.name,
                                                                                   Activity.objects.raw(
                                                                                       users_mesicni_prehled_dokoncenych_sql(
                                                                                           request, dependent_users)),
                                                                                   now)
    mesicni_prehled_dokoncenych_aktivit_top_pocet = graf_mesicni_prehled_dokoncenych_aktivit_top_pocet(request,
                                                                                                       "Všichni členové týmu " + g.name,
                                                                                                       Activity.objects.raw(
                                                                                                           users_top_pocet_sql(
                                                                                                               request,
                                                                                                               dependent_users)),
                                                                                                       now)
    mesicni_prehled_dokoncenych_aktivit_top_cas = graf_mesicni_prehled_dokoncenych_aktivit_top_cas(request,
                                                                                                   "Všichni členové týmu " + g.name,
                                                                                                   Activity.objects.raw(
                                                                                                       users_top_cas_sql(
                                                                                                           request,
                                                                                                           dependent_users)),
                                                                                                   now)
    srovnani_zpracovanych_pozadavku = graf_srovnani_zpracovanych_pozadavku_po_dnech(request,
                                                                                    "Všichni členové týmu " + g.name,
                                                                                    Activity.objects.raw(
                                                                                        users_srovnani_zpracovanych_pozadavku_sql(
                                                                                            request, dependent_users)),
                                                                                    now)
    mesicni_prehled_kapacit = graf_mesicni_prehled_kapacit(request, "Všichni členové týmu " + g.name,
                                                           Activity.objects.raw(
                                                               users_mesicni_prehled_kapacit_sql(request,
                                                                                                 dependent_users)), now)
    mesicni_prehled_efektivni = graf_kapacity_efektivni_cas(request, "Všichni členové týmu " + g.name,
                                                            Activity.objects.raw(
                                                                users_mesicni_prehled_efektivni_sql(request,
                                                                                                    dependent_users)),
                                                            now)
    rozpad = graf_rozpad_pracovni_doby(request, "Všichni členové týmu " + g.name, Activity.objects.raw(
        user_dependent_prehled_rozpad_doby_sql(request, dependent_users)), now)
    rozpad_detail = graf_rozpad_pracovni_doby(request, "Všichni členové týmu " + g.name, Activity.objects.raw(
        user_dependent_prehled_rozpad_doby_sql_detail(request, dependent_users)), now)
    mesicni_prehled_ztrata = graf_ztrata_efektivity(request, "Všichni členové týmu " + g.name,
                                                    Activity.objects.raw(
                                                        users_mesicni_prehled_ztrata_sql(request, dependent_users)),
                                                    now)
    rizeni_vykonnosti = graf_rizeni_vykonnosti(request, "Všichni členové týmu " + g.name,
                                               Activity.objects.raw(
                                                   users_dependent_rizeni_vykonnosti_sql(request, dependent_users)),
                                               now)

    graph = {
        'prehled_aktivit': prehled_aktivit,
        'mesicni_prehled_dokoncenych_aktivit_top_pocet': mesicni_prehled_dokoncenych_aktivit_top_pocet,
        'mesicni_prehled_dokoncenych_aktivit_top_cas': mesicni_prehled_dokoncenych_aktivit_top_cas,
        'mesicni_prehled_dokoncenych_aktivit': mesicni_prehled_dokoncenych_aktivit,
        'srovnani_zpracovanych_pozadavku': srovnani_zpracovanych_pozadavku,
        'mesicni_prehled_kapacit': mesicni_prehled_kapacit,
        'kapacity_efektivni_cas': mesicni_prehled_efektivni,
        'ztrata_efektivity': mesicni_prehled_ztrata,
        'rizeni_vykonnosti': rizeni_vykonnosti,
        'rozpad_pracovni_doby': rozpad,
        'rozpad_pracovni_doby_detail': rozpad_detail,
    }


    return render(request, "osvald/group_all_dependend_users_view.html",
                  {"manager": manager, 'user_info': group,'graf':graph,'users': dependent_users  })


# ==========>detail

@login_required
def graph_detail(request, name):
    manager = managers(request)
    now = datetime.datetime.now().strftime('%d.%m.%Y')
    users_id=0
    if request.method == 'GET':
        if "users" in request.GET:
            users_id=request.GET['users']
        if request.GET.get("updated_at_0"):
            start_date=request.GET['updated_at_0']
        else:
            start_date=(datetime.datetime.now() + relativedelta(day=1)).strftime("%Y-%m-%d")


        if request.GET.get("updated_at_1"):
            end_date = request.GET['updated_at_1']
        else:
            end_date = (datetime.datetime.now() + relativedelta(day=1, months=+1, days=-1)).strftime("%Y-%m-%d")


        graph = {}
        data=""
        if(name=="prehled_aktivit"):
            data = Activity.objects.raw(detail_actitvity_sql(request, users_id, start_date, end_date))
            v = graf_prehled_aktivit_detail(request, "",
                                                     Activity.objects.raw(
                                                         detail_prehled_kapacit_sql(request, users_id,start_date,end_date)), now)
            graph["graf"]=v

        if (name == "mesicni_prehled_dokoncenych_aktivit_top_pocet"):
            data = Activity.objects.raw(detail_top_pocet_sql_table(request, users_id, start_date, end_date))
            v = graf_mesicni_prehled_dokoncenych_aktivit_top_pocet_detail(request, "",
                                            Activity.objects.raw(
                                                detail_top_pocet_sql(request, users_id, start_date, end_date)), now)
            graph["graf"]=v

        if (name == "mesicni_prehled_dokoncenych_aktivit_top_cas"):
            data = Activity.objects.raw(detail_top_cas_sql_table(request, users_id, start_date, end_date))
            v = graf_mesicni_prehled_dokoncenych_aktivit_top_cas_detail(request, "",
                                            Activity.objects.raw(
                                                detail_top_cas_sql(request, users_id, start_date, end_date)), now)
            graph["graf"]=v




        table = DetailTable(data)

        table.order_by = '-updated_at'
        RequestConfig(request,paginate={'per_page':15}).configure(table)

        filter = DateFilter(request.GET, queryset=data)

        export_format = request.GET.get('_export', None)
        if TableExport.is_valid_format(export_format):
            for d in table.data:
                d.created_at =  d.created_at.strftime("%d.%m %Y")
                d.updated_at =  d.updated_at.strftime("%d.%m %Y")

            exporter = TableExport(export_format, table)
            return exporter.response('ctable.{}'.format(export_format))

        return render(request, "osvald/graph_detail.html",
                      {"manager": manager, 'info': name,'table':table,'graf':graph,'filter': filter,"users": users_id,})


@login_required
def kapacity_detail(request):
    manager = managers(request)
    now = datetime.datetime.now().strftime('%d.%m.%Y')

    if request.method == 'GET':
        users=request.GET['users']



    data = Activity.objects.raw(detail_kapacity_sql(request, users))



    table = DetailKapacityTable(data)
   
    RequestConfig(request,paginate={'per_page':15}).configure(table)


    export_format = request.GET.get('_export', None)
    if TableExport.is_valid_format(export_format):
        exporter = TableExport(export_format, table)
        return exporter.response('ctable.{}'.format(export_format))

    return render(request, "osvald/kapacity_detail.html",
                  {"manager": manager,'table':table,"users":users,})


# =============> Object functions

def dependent_groups(request):
    return Group.objects.get(manager=request.user).get_descendants(include_self=True);


def superior_groups(request):
    try:
        myGroupID = Group.objects.all().filter(user=request).first().id

    except:
        myGroupID = 0
    try:

        return Group.objects.get(id=myGroupID).get_ancestors(include_self=True);
    except Group.DoesNotExist:
        return False


def dependent_users(request):
    dependent_users = []
    for item in dependent_groups(request):

        for u in item.user.all():
            dependent_users.append(u)

    return dependent_users


def is_manager(request):
    try:
        manager = Group.objects.get(manager=request)
        return True
    except Group.DoesNotExist:
        return False


def is_my_manager(user, manager):

    superior_users = []

    if superior_groups(user):
        for item in superior_groups(user):
            for u in item.manager.all():
                superior_users.append(u)


    if manager in superior_users:
        return True
    else:
        return False

    # return superior_users


# =============> Ajax Views

@login_required
def new_activity(request):
    csrfContext = RequestContext(request)

    ActivityObj = Activity.objects.filter(status_id=1,username_id=request.user.id)

    if len(ActivityObj)>0:
        return HttpResponse('is_running')
    if not request.user.is_authenticated:
        return HttpResponse('unauthorized')
    if request.method == "POST":

        form = StartActivity(request.POST)

        if form.is_valid():

            post = form.save(commit=False)

            post.status_id = 1
            post.template_id = request.POST.get('template_id')
            # post.document_id = request.POST.get('document_id')
            post.total_time = request.POST.get('total_time')
            post.activity_count = request.POST.get('activity_count')
            post.username = request.user


            post.save()

            request.session['running_activity'] = request.POST.get('template_id')
            request.session['running_activity_id'] = post.id
            return HttpResponse(post.id)

        else:

            return redirect('/')


@login_required
def test_data(request):
    csrfContext = RequestContext(request)
    if not request.user.is_authenticated:
        return HttpResponse('unauthorized')
    if request.method == "POST":

        for x in range(0, 1000):
            form = StartActivity(request.POST)


            if form.is_valid():
                randomdatum = datetime.date.today() - timedelta(days=random.randrange(-10, 3))

                template_id = randint(3, 80)

                post = form.save(commit=False)

                post.status_id = 3
                post.template_id = template_id
                post.customer_id = "test"
                post.total_time = '00:00:' + str(randint(10, 59))
                post.activity_count = randint(1, 10)
                post.updated_at = timezone.now() - timedelta(days=random.randrange(1, 400))
                # post.username = request.user
                post.username = User.objects.get(id=randint(3, 27))
                post.document_id = "test" + str(randint(3, 27))
                post.post_id = "test"


                post.save()




    return  ""


def random_date(start, end):
    """
    This function will return a random datetime between two datetime
    objects.
    """
    delta = end - start
    int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
    random_second = randrange(int_delta)
    return start + timedelta(seconds=random_second)


@login_required
def pause_activity(request):
    csrfContext = RequestContext(request)
    if not request.user.is_authenticated:
        return HttpResponse('unauthorized')
    if request.method == "POST":
        ActivityObj = Activity.objects.get(id=request.POST.get("id"))
        if "total_time" in request.POST:
            ActivityObj.total_time = request.POST.get("total_time")

        ActivityObj.status_id = 2
        ActivityObj.comment = request.POST.get("comment")
        ActivityObj.save()
        return HttpResponse(ActivityObj.id)

    return render(request, 'osvald/mereni.html')


@login_required
def continue_activity(request):
    csrfContext = RequestContext(request)
    if not request.user.is_authenticated:
        return HttpResponse('unauthorized')
    if request.method == "POST":
        ActivityObj = Activity.objects.get(id=request.POST.get("id"))

        ActivityObj.status_id = 1
        ActivityObj.comment = request.POST.get("comment")
        ActivityObj.save()

        return HttpResponse(ActivityObj.id)

    return render(request, 'osvald/mereni.html')


@login_required
def end_activity(request):
    csrfContext = RequestContext(request)
    if not request.user.is_authenticated:
        return HttpResponse('unauthorized')
    if request.method == "POST":

        ActivityObj = Activity.objects.get(id=request.POST.get("id"))
        if "total_time" in request.POST:
            ActivityObj.total_time = request.POST.get("total_time")
        if "subtype" in request.POST and request.POST.get("subtype")!="" and request.POST.get("subtype")!= None:

            ActivityObj.subtype = Activity_subtype.objects.get(id=request.POST.get("subtype"))

        ActivityObj.status_id = 3
        ActivityObj.activity_count = request.POST.get("activity_count")
        ActivityObj.comment = request.POST.get("comment")
        ActivityObj.save()
        a= Collections()



        if request.POST.get("selector") != None:

            if request.POST.get("selector")!="":
                s = Selector_type_list.objects.get(id=request.POST.get("selector"))

                
                if request.POST.get("value")=="" or request.POST.get("value")== None or request.POST.get("value")== "NaN" :
                    value = 0;
                else:
                    value = request.POST.get("value")
    
                if request.POST.get("date_to_pay") != "":
                    a = Module_table_collections.objects.create(user=request.user,selector=s,document_id=ActivityObj.document_id,customer_id=ActivityObj.customer_id,value=value,date_to_pay=datetime.datetime.strptime(request.POST.get("date_to_pay"), "%d.%m.%Y"),description=request.POST.get("description"),date_created=timezone.now())
                else:
                    a = Module_table_collections.objects.create(user=request.user,selector=s,document_id=ActivityObj.document_id,customer_id=ActivityObj.customer_id,value=value,description=request.POST.get("comment"),date_created=timezone.now())

        return HttpResponse(ActivityObj.id)


    return render(request, 'osvald/mereni.html')


@login_required
def get_time(request):
    if not request.user.is_authenticated:
        return HttpResponse('unauthorized')
    if request.method == "GET":
        if "id" in request.GET:
            ActivityObj = Activity.objects.get(id=request.GET.get("id"))
            return HttpResponse(str(ActivityObj.total_time))
    else:

        return redirect('/')

    return render(request, 'osvald/mereni.html')



def get_authorized(request):
    if not request.user.is_authenticated:
        return HttpResponse('false')
    else:
        return HttpResponse('true')

@login_required
def update_activity(request):
    if request.method == "POST":

        ActivityObj = Activity.objects.get(id=request.POST.get("id"))
        if "total_time" in request.POST:
            ActivityObj.total_time = request.POST.get("total_time")
        ActivityObj.save()

    return render(request, 'osvald/mereni.html')


@login_required
def update_resolution(request):
    if request.is_ajax() or request.method == 'POST':
        request.session['width'] = request.POST.get("x")
        request.session['height'] = request.POST.get("y")

        return HttpResponse('ok')


# =============> Views
from django.shortcuts import render_to_response



from django.template import RequestContext




def collectionsList(request):
    manager = managers(request)

    table = CollectionsTable(Module_table_collections.objects.all())
    table.order_by = '-date_created'
    RequestConfig(request,paginate={'per_page':25}).configure(table)
    filterset_class = Module_table_collections

    export_format = request.GET.get('_export', None)
    if TableExport.is_valid_format(export_format):
        exporter = TableExport(export_format, table)
        return exporter.response('ctable.{}'.format(export_format))

    return render(request, 'osvald/module_table_collections_list.html', {"manager": manager,'table': table})

def login(request):
    """Blank page.
    """
    return render(request, "osvald/login.html",
                  {"nav_active": "blank"})


def home(request):
    """Blank page.
    """
    return render(request, "osvald/home.html",
                  {"nav_active": "home"})


@login_required
def mereni(request):
    request.session.modified = True
    logical_teams = Logical_team.objects.filter(user=request.user).all()
    try:
        primary_team = request.user.user_logical_team.team.id
    except:
        print("neni")

    if request.GET:
        if "active_team" in request.GET:
            request.session['active_team'] = request.GET.get('active_team')

    else:
        request.session['active_team'] = primary_team

    if is_manager(request.user):
        manager = {"superior": superior_groups(request), "dependent": dependent_groups(request),
                   "dependent_users": dependent_users(request), }
    else:
        manager = False

    #     userdata = Employee.objects.get(username=user, password=passw)
    #     user_id = request.session["user_id"] = userdata.id
    #     employee_details = Employee.objects.get(id=user_id)

    # return redirect('login')
    # except Employee.DoesNotExist:
    #     state = "Username or password incorrect !"
    #     return render_to_response('login.html',
    #                               {'username': username1, 'state': state},
    #                               context_instance=RequestContext(request))

    if "active_team" in request.session:

        active_team = int(request.session['active_team'])
    else:
        active_team = 0

    if active_team != 0:
        # categories = Category_activity.objects.filter(logical_team__in=active_team).prefetch_related('template_activity_set').all().prefetch_related('template_activity_set__avg_activity_set').all()
        categories = Category_activity.objects.prefetch_related('logical_team_set').filter(logical_team=active_team).all()

    paused = Activity.objects.filter(status=2).filter(username=request.user).all().select_related('template').all()

    # active = Activity.objects.filter(status=1).filter(username=request.user).all().select_related('template').all()

    active = Activity.objects.filter(status=1).filter(username=request.user).all().select_related('template').first()

    subtype = Activity_subtype.objects.all()
    # active_category=Activity.objects.filter(status=1).filter(username=request.user).all().select_related('template__id_category').all()
    try:
        active_category = Activity.objects.filter(status=1).filter(username=request.user).all().select_related(
            'template__id_category').all().first().template.id_category
    except AttributeError:
        active_category = categories.first()


    teams = {'teams': logical_teams, 'selected_team': active_team, 'primary_team': primary_team}
    return render(request, "osvald/mereni.html",
                  {"nav_active": "home", 'teams': teams, 'paused': paused, 'categories': categories, 'active': [active],
                   'subtype': subtype, 'manager': manager, "active_category": active_category})


def user_activity_view(request, username):
    if is_manager(request.user):
        manager = {"superior": superior_groups(request), "dependent": dependent_groups(request),
                   "dependent_users": dependent_users(request), }
    else:
        manager = False

    now = datetime.datetime.now().strftime('%d.%m.%Y')
    try:
        u = User.objects.get(username=username)
    except:
        raise Http404("Uživatel neexistuje")



    if ((str(request.user) != str(u.username)) and not is_my_manager(u, request.user)):
        raise PermissionDenied

    # pocitat podle posledního update aktivity do měsíce- pouze dokončené





    prehled_aktivit = graf_prehled_aktivit(request, u.username, Activity.objects.raw(user_prehled_activit_sql(request,u)), now)
    mesicni_prehled_dokoncenych_aktivit = graf_mesicni_prehled_dokoncenych_aktivit(request, u.username, Activity.objects.raw(user_mesicni_prehled_dokoncenych_sql(request,u)),now)

    mesicni_prehled_dokoncenych_aktivit_top_pocet = graf_mesicni_prehled_dokoncenych_aktivit_top_pocet(request, u.username,Activity.objects.raw(user_top_pocet_sql(request,u)),now)
    mesicni_prehled_dokoncenych_aktivit_top_cas = graf_mesicni_prehled_dokoncenych_aktivit_top_cas(request, u.username,Activity.objects.raw(user_top_cas_sql(request, u)),now)
    srovnani_zpracovanych_pozadavku = graf_srovnani_zpracovanych_pozadavku_po_dnech(request, u.username,Activity.objects.raw(user_srovnani_zpracovanych_pozadavku_sql(request,u)),now)

    mesicni_prehled_kapacit = graf_mesicni_prehled_kapacit(request, u.username, Activity.objects.raw(user_mesicni_prehled_kapacit_sql(request,u)), now)

    kapacity_efektivni_cas = graf_kapacity_efektivni_cas(request, u.username, Activity.objects.raw(user_mesicni_prehled_efektivni_cas_sql(request,u)), now)
    ztrata_efektivity = graf_ztrata_efektivity(request, u.username, Activity.objects.raw(user_mesicni_prehled_ztrata_efektivity_sql(request,u)), now)

    rozpad_pracovni_doby = graf_rozpad_pracovni_doby(request, u.username, Activity.objects.raw(user_mesicni_prehled_rozpad_doby_sql(request,u)), now)
    rozpad_pracovni_doby_detail = graf_rozpad_pracovni_doby(request, u.username, Activity.objects.raw(user_mesicni_prehled_rozpad_doby_sql_detail(request,u)), now)

    graf = {


        'rozpad_pracovni_doby': rozpad_pracovni_doby,
        'rozpad_pracovni_doby_detail': rozpad_pracovni_doby_detail,
        'ztrata_efektivity': ztrata_efektivity,
        'kapacity_efektivni_cas': kapacity_efektivni_cas,
        'prehled_aktivit': prehled_aktivit,
        'mesicni_prehled_kapacit': mesicni_prehled_kapacit,
        'mesicni_prehled_dokoncenych_aktivit_top_pocet': mesicni_prehled_dokoncenych_aktivit_top_pocet,
        'mesicni_prehled_dokoncenych_aktivit_top_cas': mesicni_prehled_dokoncenych_aktivit_top_cas,
        'mesicni_prehled_dokoncenych_aktivit': mesicni_prehled_dokoncenych_aktivit,
        'srovnani_zpracovanych_pozadavku': srovnani_zpracovanych_pozadavku,
    }
    return render(request, "osvald/user_activity.html",
                  {
                      "manager": manager,
                      'user_info': u,
                      'graf': graf,
                      'users': [request.user.id],
                      # 'souhrn': overview_chart, 'dochazka': dochazka_chart,
                      # 'primary_team_activity': primary_team_activity
                  })


def test(request):
    """Blank page.
    """

    superior_groups = Group.objects.get(manager=request.user).get_ancestors(include_self=False);
    dependent_groups = Group.objects.get(manager=request.user).get_descendants(include_self=True);
    return render(request, "osvald/user_activity2.html",
                  {"superior": superior_groups, "dependent": dependent_groups})

def error_404(request):
    data = {}
    return render(request, 'osvald/404.html', data)


def error_403(request):
    data = {}
    return render(request, 'osvald/403.html', data)


def error_500(request):
    data = {}
    return render(request, 'osvald/500.html', data)

