from graphos.sources.simple import SimpleDataSource
from graphos.sources.model import ModelDataSource
from graphos.renderers.gchart import PieChart, LineChart, ColumnChart, ComboChart, CalendarChart
from datetime import timedelta
from dateutil.relativedelta import relativedelta


def get_total_width(request):
    if "width" in request.session:
        width = request.session['width']
    else:
        width = 1280
    if int(width) > 120:
        return str(int(width) - 120)


def get_half_width(request):
    if "width" in request.session:
        width = request.session['width']
    else:
        width = 1280
    if int(width) > 120:
        return str(int(width) / 2 - 120)


def get_30_width(request):
    if "width" in request.session:
        width = request.session['width']
    else:
        width = 1280
    if int(width) > 120:
        return str(int(width) / 3 - 120)



def graf_prehled_aktivit(request, name, data, date):
    source = ModelDataSource(data, fields=['Aktivita', 'Dokončené',"Pozastavené","Můj průměr","Průměr týmu"])
    print(source)
    return ComboChart(source,
                      options={'title': "Uživatelský přehled aktivit: " + name + " k datu " + date,
                               'isStacked': 'false',
                               'hAxis': {'title': 'Název aktivity', 'slantedText': 'true', 'slantedTextAngle': 60, 'textStyle' :{'fontSize':'10'}},
                               'legend': {'position': 'bottom', 'maxLines': 5},
                               'vAxes': {0: {'title': 'Počet kusů'}, 1: {'title': 'Průměrný čas v minutách'}},
                               'colors': ['#66AA00', '#3366CC', '#000000', '#e2431e'], 'seriesType': 'bars',
                               'series': {0: {'type': 'bars', 'targetAxisIndex': 0},
                                          1: {'type': 'bars', 'targetAxisIndex': 0},
                                          2: {'type': 'line', 'targetAxisIndex': 1},
                                          3: {'type': 'line', 'targetAxisIndex': 1,
                                              'lineDashStyle': [2, 2, 20, 2, 20, 2]}},
                               'width': get_total_width(request), 'height': '140%',
                               'explorer': {'axis': 'horizontal', 'action': ['dragToZoom', 'rightClickToReset'],
                                            'keepInBounds': 'true',
                                            'maxZoomIn': 4.0},
                               'chartArea': {'width': 'chartwidth',  "height": "45%", "top": '10'}})

def graf_prehled_aktivit_group(request, name, data, date):
    source = ModelDataSource(data, fields=['Aktivita', 'Dokončené',"Pozastavené","Týmový průměr","Nejlepší v týmu"])
    print(source)
    return ComboChart(source,
                      options={'title': "Uživatelský přehled aktivit: " + name + " k datu " + date,
                               'isStacked': 'false',
                               'hAxis': {'title': 'Název aktivity', 'slantedText': 'true', 'slantedTextAngle': 60, 'textStyle' :{'fontSize':'10'}},
                               'legend': {'position': 'bottom', 'maxLines': 5},
                               'vAxes': {0: {'title': 'Počet kusů'}, 1: {'title': 'Průměrný čas v minutách'}},
                               'colors': ['#66AA00', '#3366CC', '#000000', '#e2431e'], 'seriesType': 'bars',
                               'series': {0: {'type': 'bars', 'targetAxisIndex': 0},
                                          1: {'type': 'bars', 'targetAxisIndex': 0},
                                          2: {'type': 'line', 'targetAxisIndex': 1},
                                          3: {'type': 'line', 'targetAxisIndex': 1,
                                              'lineDashStyle': [2, 2, 20, 2, 20, 2]}},
                               'width': get_total_width(request), 'height': '800px',
                               'explorer': {'axis': 'horizontal', 'action': ['dragToZoom', 'rightClickToReset'],
                                            'keepInBounds': 'true',
                                            'maxZoomIn': 4.0},
                               'chartArea': {'width': 'chartwidth',  "height": "45%", "top": '10'}})


def graf_mesicni_prehled_dokoncenych_aktivit(request, name, data, date):
    source = ModelDataSource(data, fields=['Měsíc', 'Počet dokončených aktivit'])
    return ColumnChart(source,
                       options={'title': "Měsíční přehled dokončených aktivit: " + name + " k datu " + date,
                                'isStacked': 'true', 'legend': {'position': 'bottom', 'maxLines': 3},
                                'hAxis': {'title': 'Měsíc'}, 'vAxis': {'title': 'Počet kusů'},
                                'colors': ['#66AA00', '#3366CC'], 'chartArea': {'width': '95%'},
                                'width': get_total_width(request)})


def graf_mesicni_prehled_dokoncenych_aktivit_top_pocet(request, name, data, date):
    source = ModelDataSource(data, fields=['Aktivita', 'Počet'])
    return ColumnChart(source,
                       options={'title': "Top 10 aktivit dle počtu: " + name + " k datu " + date,
                                'isStacked': 'true',
                                'legend': {'position': 'bottom', 'maxLines': 3}, 'hAxis': {'title': 'Název aktivity','slantedTextAngle': 60, 'textStyle' :{'fontSize':'10'}},
                                'vAxis': {'title': 'Počet kusů'}, 'colors': ['#3366CC'],
                                'width': get_half_width(request),
                                'chartArea': {'width': 'chartwidth', "height": "45%", "top": '10'}})


def graf_mesicni_prehled_dokoncenych_aktivit_top_cas(request, name, data, date):
    source = ModelDataSource(data, fields=['Aktivita', 'Počet'])
    return ColumnChart(source,
                       options={'title': "Top 10 aktivit dle času: " + name + " k datu " + date,
                                'isStacked': 'true',
                                'legend': {'position': 'bottom', 'maxLines': 3}, 'hAxis': {'title': 'Název aktivity','slantedText': 'true', 'slantedTextAngle': 60, 'textStyle' :{'fontSize':'10'}},
                                'vAxis': {'title': 'Alokovaný čas v hodinách'}, 'colors': ['#3366CC'],
                                'width': get_half_width(request),
                                'chartArea': {'width': 'chartwidth', "height": "45%", "top": '10'}})


def graf_mesicni_prehled_dokoncenych_aktivit_top_pocet_detail(request, name, data, date):
    source = ModelDataSource(data, fields=['Aktivita', 'Počet'])
    return ColumnChart(source,
                       options={'title': "Top 10 aktivit dle počtu: " + name + " k datu " + date,
                                'isStacked': 'true',
                                'legend': {'position': 'bottom', 'maxLines': 3}, 'hAxis': {'title': 'Název aktivity'},
                                'vAxis': {'title': 'Počet kusů'}, 'colors': ['#3366CC'],
                                'width': get_total_width(request)})


def graf_mesicni_prehled_dokoncenych_aktivit_top_cas_detail(request, name, data, date):
    source = ModelDataSource(data, fields=['Aktivita', 'Počet'])
    return ColumnChart(source,
                       options={'title': "Top 10 aktivit dle času: " + name + " k datu " + date,
                                'isStacked': 'true',
                                'legend': {'position': 'bottom', 'maxLines': 3}, 'hAxis': {'title': 'Název aktivity'},
                                'vAxis': {'title': 'Alokovaný čas v hodinách'}, 'colors': ['#3366CC'],
                                'width': get_total_width(request)})


def graf_srovnani_zpracovanych_pozadavku_po_dnech(request, name, data, date):

    source = ModelDataSource(data, fields=["Den v měsíci", "Počet současný měsíc","Počet minulý měsíc","Počet minulý rok"])
    return ComboChart(source,
                      options={
                          'title': "Srovnání zpracovaných požadavků po dnech: " + name + " k datu " + date,
                          'isStacked': 'false', 'hAxis': {'title': 'Den v měsíci'},'pointSize': 5,
                          'legend': {'position': 'bottom', 'maxLines': 5},
                          'vAxes': {0:{'title': 'Počet aktivit'}},
                          'colors': ['#3366CC', '#000000','#66AA00'],
                          'seriesType': 'line',
                          'series': {0: {'type': 'line', 'targetAxisIndex': 0},
                                     1: {'type': 'line', 'targetAxisIndex': 0},
                                     2: {'type': 'line', 'targetAxisIndex': 0}},

                          'width': get_total_width(request),
                          'chartArea': {'width': get_total_width(request), "left":60, "top": 55}})


def graf_mesicni_prehled_kapacit(request, name, data, date):
    source = ModelDataSource(data, fields=['Měsíc', 'Čistý pracovní čas','ANET odpracováno'])
    return ComboChart(source,
                      options={'title': "Měsíční přehled kapacit: " + name + " k datu " + date,
                               'isStacked': 'false','pointSize': 5,
                               'hAxis': {'title': 'Měsíc'}, 'legend': {'position': 'bottom', 'maxLines': 5},
                               'vAxes': {0: {'title': 'Počet hodin'}}, 'colors': ['#66AA00', '#000000'],
                               'seriesType': 'bars', 'series': {0: {'type': 'bars', 'targetAxisIndex': 0},
                                                                1: {'type': 'line', 'targetAxisIndex': 0}},
                               'width': get_total_width(request)})


def graf_kapacity_efektivni_cas(request, name, data, date):
    source = ModelDataSource(data, fields=['Měsíc', 'Efektivní čas','Procent z fondu pracovní doby'])
    return ComboChart(source,
                      options={'title': "Měsíční přehled kapacit - efektivní čas: " + name + " k datu " + date,
                               'isStacked': 'false', 'hAxis': {'title': 'Měsíc'},'pointSize': 5,
                               'legend': {'position': 'bottom', 'maxLines': 5},
                               'vAxes': {0: {'title': 'Počet hodin'}, 1: {'title': 'Procent (%)'}},

                               'colors': ['#3366CC', '#000000'], 'seriesType': 'bars',
                               'series': {0: {'type': 'bars', 'targetAxisIndex': 0},
                                          1: {'type': 'line', 'targetAxisIndex': 1}}, 'width': get_30_width(request),
                               'chartArea': {'width': get_30_width(request), "top": 55}})


def graf_ztrata_efektivity(request, name, data, date):
    source = ModelDataSource(data, fields=['Měsíc', 'Ztráta efektivity','Procent z fondu pracovní doby'])
    return ComboChart(source,
                      options={
                          'title': "Měsíční přehled kapacit - ztráta efektivity: " + name + " k datu " + date,
                          'isStacked': 'false', 'hAxis': {'title': 'Měsíc'},'pointSize': 5,
                          'legend': {'position': 'bottom', 'maxLines': 5},
                          'vAxes': {0: {'title': 'Počet hodin'}, 1: {'title': 'Procent (%)'}},
                          'colors': ['#e2431e', '#000000'], 'seriesType': 'bars',
                          'series': {0: {'type': 'bars', 'targetAxisIndex': 0},
                                     1: {'type': 'line', 'targetAxisIndex': 1}}, 'width': get_30_width(request),
                          'chartArea': {'width': get_30_width(request), "top": 55}})


def graf_rozpad_pracovni_doby(request, name, data, date):
    source = ModelDataSource(data, fields=["nadpis", 'value'])
    from datetime import datetime

    datetime_object = datetime.strptime(date, '%d.%m.%Y')
    last_day_of_the_month = datetime(datetime_object.year, (datetime_object + relativedelta(months=0)).month, 1) - timedelta(days=1)


    return PieChart(source,
                    options={'title': "Rozpad pracovní doby: " + name + " k datu " + str(last_day_of_the_month.strftime('%d.%m.%Y')), 'pieHole': 0.4,
                             'legend': {'position': 'bottom', 'maxLines': 5}, 'width': get_30_width(request),
                           #  'colors': ['#3366CC','#f1ca3a','#000000', '#e2431e',  ],
                             'colors': ['#e2431e',  '#3366CC','#f1ca3a', '#000000', ],
                             'chartArea': {'width': get_30_width(request), "top": 55}})

def graf_rizeni_vykonnosti(request, name, data, date):
    source = ModelDataSource(data, fields=["Uživatel", 'Efektivní čas','Podpůrné aktivity','Ztráta efektivity'])

    from datetime import datetime

    datetime_object = datetime.strptime(date, '%d.%m.%Y')
    last_day_of_the_month = datetime(datetime_object.year, (datetime_object + relativedelta(months=0)).month, 1) - timedelta(days=1)

    return ColumnChart(source,
                      options={'title': "Měsíční přehled k výkonnosti zaměstnanců k datu " + str(last_day_of_the_month.strftime('%d.%m.%Y')),
                               'isStacked': 'true',
                               'hAxis': {'title': 'Jméno zaměstnance', 'slantedText': 'true', 'slantedTextAngle': 60, 'textStyle' :{'fontSize':'10'}},
                               'legend': {'position': 'bottom', 'maxLines': 5},
                               'vAxes': {0: {'title': 'Počet hodin'}},
                               'colors': ['#3366CC','#f1ca3a',  '#e2431e', '#000000'], 'seriesType': 'bars',
                               # series': {0: {'type': 'bars', 'targetAxisIndex': 0},
                               #            1: {'type': 'bars', 'targetAxisIndex': 0},
                               #            2: {'type': 'bars', 'targetAxisIndex': 1},
                               #            3: {'type': 'line', 'targetAxisIndex': 1,
                               #                'lineDashStyle': [2, 2, 20, 2, 20, 2]}},
                               'width': get_total_width(request), 'height': '140%',
                               'explorer': {'axis': 'horizontal', 'action': ['dragToZoom', 'rightClickToReset'],
                                            'keepInBounds': 'true',
                                            'maxZoomIn': 4.0},
                               'chartArea': {'width': 'chartwidth', "height": "50%", "top": 55}})



def graf_prehled_aktivit_detail(request, name, data, date):
    source = ModelDataSource(data, fields=['Aktivita', 'Dokončené',"Pozastavené","Průměr","Nejlepší v týmu"])
    print(source)
    return ComboChart(source,
                      options={'title': "Uživatelský přehled aktivit: " + name + " k datu " + date,
                               'isStacked': 'false','pointSize': 5,'textStyle' :{'fontSize':'10'},
                               'hAxis': {'title': 'Název aktivity', 'slantedText': 'true', 'slantedTextAngle': 60, 'textStyle' :{'fontSize':'10'}},
                               'legend': {'position': 'bottom', 'maxLines': 5,'textStyle' :{'fontSize':'10'}},
                               'vAxes': {0: {'title': 'Počet kusů'}, 1: {'title': 'Průměrný čas v minutách'},'textStyle' :{'fontSize':'10'}},
                               'colors': ['#66AA00', '#3366CC', '#000000', '#e2431e'], 'seriesType': 'bars',
                               'series': {0: {'type': 'bars', 'targetAxisIndex': 0},
                                          1: {'type': 'bars', 'targetAxisIndex': 0},
                                          2: {'type': 'line', 'targetAxisIndex': 1},
                                          3: {'type': 'line', 'targetAxisIndex': 1,
                                              'lineDashStyle': [2, 2, 20, 2, 20, 2]}},
                               'width': get_total_width(request), 'height': '800px',
                               'explorer': {'axis': 'horizontal', 'action': ['dragToZoom', 'rightClickToReset'],
                                            'keepInBounds': 'true',
                                            'maxZoomIn': 4.0},
                               'chartArea': {'width': 'chartwidth', "height": "45%", "top": '10'}})
