from django import forms
from .models import *
from django.forms import ModelForm
import csv

class TeamChoiceField(forms.Form):

    teams = forms.ModelChoiceField(
        queryset=Logical_team.objects.all()

    )


class Collections(forms.ModelForm):
    class Meta:
        model = Module_table_collections
        fields = ('value', 'date_to_pay',)


class StartActivity(forms.ModelForm):
     class Meta:
         model = Activity
         fields = ('customer_id', 'document_id',)


class ReStartActivity(forms.ModelForm):
     class Meta:
         model = Activity
         fields = ('id','comment',)





class DataImport(ModelForm):
    file_to_import = forms.FileField()

    class Meta:
        model = Anet_activity
        fields = ("file_to_import",)

    def save(self, commit=False, *args, **kwargs):
        form_input = DataImport()
       ## self.place = self.cleaned_data['place']
        file_csv = self.request.FILES['file_to_import']
        datafile = open(file_csv, 'rb')
        records = csv.reader(datafile)
        for line in records:
            self.time = line[1]
            self.data_1 = line[2]
            self.data_2 = line[3]
            self.data_3 = line[4]
            form_input.save()
        datafile.close()
