from django.conf.urls import url
from . import views
from django.contrib.auth import views as auth_views
from osvald.views import collectionsList


urlpatterns = [
    url(r'^login/$', auth_views.login, {'template_name': 'osvald/login.html'}, name='login'),
    url(r'^logout/$', auth_views.logout, {'next_page': '/login/'}, name='logout'),


    ## url(r'^$', auth_views.login, {'template_name': 'osvald/login.html'}, name='login'),
    ##url(r'^$', views.home, name='home'),
    url(r'^$', views.mereni, name='mereni'),
    url(r'^new_activity/$', views.new_activity, name='new_activity'),
    url(r'^collections/$', collectionsList,name='collectionsList'),
    url(r'^continue_activity/$', views.continue_activity, name='continue_activity'),
    url(r'^pause_activity/$', views.pause_activity, name='pause_activity'),
    url(r'^end_activity/$', views.end_activity, name='end_activity'),
    url(r'^update_activity/$', views.update_activity, name='update_activity'),
    url(r'^update_resolution/$', views.update_resolution, name='update_resolution'),
    url(r'^get_time/$', views.get_time, name='get_time'),
    url(r'^test_data/$', views.test_data, name='test_data'),
    url(r'^get_authorized/$', views.get_authorized, name='get_authorized'),

    # user activity view
    url(r'^user/(?P<username>\w+)/$', views.user_activity_view, name='user_activity_view'), ##> ok
    url(r'^user/(?P<username>.+)/users/dependend/$', views.user_all_dependend_users_view, name='user_all_dependend_users_view'),#OK
    url(r'^user/(?P<username>.+)/logic_team/dependend/$', views.user_all_dependend_logic_view, name='user_all_dependend_logic_view'),#ok
    url(r'^user/(?P<username>.+)/group/dependend/$', views.user_all_dependend_group_view, name='user_all_dependend_group_view'),#ok

    # user activity view
    url(r'^group/(?P<group>.+)/users/$', views.group_all_dependend_users_view, name='group_all_dependend_users_view'),#ok
    url(r'^group/(?P<group>.+)/dependend/$', views.group_all_dependend_group_view, name='group_all_dependend_group_view'),
    url(r'^group/(?P<group>.+)/logic_team/$', views.group_all_dependend_logic_view, name='group_all_dependend_logic_view'),
    url(r'^group/(?P<group>.+)/$', views.group_activity_view, name='group_activity_view'),  # ok

    url(r'^logic_team/(?P<name>.+)/$', views.logical_team_activity_view, name='logical_team_activity_view'), #ok

    url(r'^graph/(?P<name>.+)$', views.graph_detail, name='graph_detail'),
    url(r'^kapacity/$', views.kapacity_detail, name='kapacity_detail'),

    url(r'^test/$', views.test, name='test'),

]
