import mptt
from django.db import models
from django.contrib import auth
from django.contrib.auth.models import User
from colorfield.fields import ColorField
from fontawesome.fields import IconField
from datetime import datetime
from django.db.models.signals import post_save
from django.dispatch import receiver
from mptt.fields import TreeForeignKey
from django_auth_ldap.backend import LDAPBackend
from django.utils import timezone

Activity_status = (
    (0, 'New'),
    (1, 'Paused'),
    (2, 'Closed'),
)

Full_contract = 480


class Selector_type_list(models.Model):
    name = models.CharField(max_length=200, blank=False, verbose_name="Jméno");
    decription = models.CharField(max_length=2000, blank=True, verbose_name="Popisek");
    show_form = models.BooleanField("Zobrazit formulář", default=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Hodnoty číselník'
        verbose_name_plural = 'Hodnoty číselníků'
        ordering = ["name"]


class Selector_group(models.Model):
    name = models.CharField(max_length=200, blank=False, verbose_name="Jméno");
    decription = models.CharField(max_length=2000, blank=True, verbose_name="Popisek");
    items = models.ManyToManyField(Selector_type_list, verbose_name="Položky skupiny", db_index=True)

    class Meta:
        verbose_name = 'Skupina v číselníku'
        verbose_name_plural = 'Skupiny v číselníku'

    def __str__(self):
        return self.name


class Activity_selector(models.Model):
    name = models.CharField(max_length=200, blank=False, verbose_name="Název");
    decription = models.CharField(max_length=2000, blank=True, verbose_name="Popisek");
    values = models.ManyToManyField(Selector_group, verbose_name="Položky číselníku", db_index=True)

    class Meta:
        verbose_name = 'Číselník'
        verbose_name_plural = 'Číselníky'


class Osvald_modules(models.Model):
    name = models.CharField(max_length=200, blank=False, verbose_name="Jméno modulu");
    user = models.ManyToManyField(User, verbose_name="Přiřazení uživatelé", related_name="module_user")
    table = models.CharField(max_length=200, blank=False, verbose_name="Tabulka v DB");
    selector = models.ForeignKey(Activity_selector, blank=True, null=True, on_delete=models.CASCADE,
                                 verbose_name="Číselník")

    class Meta:
        verbose_name = 'Modul'
        verbose_name_plural = 'Moduly'

    def __str__(self):
        return self.name


class Module_table_collections(models.Model):
    user = models.ForeignKey(User, db_index=True, on_delete=models.CASCADE, verbose_name="Vytvořeno")
    date_created = models.DateField(blank=True, null=True, db_index=True, verbose_name="Datum zadání")
    customer_id = models.CharField(max_length=200, blank=True, null=True, db_index=True, verbose_name="ID klienta")
    document_id = models.CharField(max_length=200, blank=True, null=True, db_index=True, verbose_name="Číslo smlouvy")
    value = models.FloatField(blank=True, null=True, db_index=True, verbose_name="Částka (Kč)")
    description = models.CharField(max_length=2000, blank=True, null=True, verbose_name="Popisek");
    date_to_pay = models.DateField(blank=True, null=True, db_index=True, verbose_name="Datum přislíbení")
    selector = models.ForeignKey(Selector_type_list, null=True, db_index=True, verbose_name="Akce ",
                                 on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Collections'
        verbose_name_plural = 'Collections'


class Category_activity(models.Model):
    name = models.CharField("Název záložky", max_length=200)
    icon = IconField("Ikona záložky")
    description = models.TextField("Popisek (nepovinné)", max_length=2000, blank=True)
    color = ColorField("Barva záložky", default='#FF0000')

    class Meta:
        verbose_name = 'Záložka'
        verbose_name_plural = 'Záložky'

    def __str__(self):
        return self.name

    def id_name(self):
        return self.name.replace(' ', '_').replace(',', '')


class Logical_team(models.Model):
    name = models.CharField('Název týmu', max_length=200)
    description = models.TextField('Popisek', blank=True, max_length=2000)
    category = models.ManyToManyField(Category_activity, verbose_name="Přiřadit záložku", blank=True, db_index=True)
    user = models.ManyToManyField(User, verbose_name="Členství uživatelů", blank=True, db_index=True)

    class Meta:
        verbose_name = 'Logický tým'
        verbose_name_plural = 'Logické týmy'

    def id_str(self):
        return str(self.id)

    def __str__(self):
        return self.name


class Group(auth.models.Group):
    manager = models.ManyToManyField(User, verbose_name="Manažer", blank=True, related_name="group_manager")
    user = models.ManyToManyField(User, verbose_name="Uživatelé", blank=True, related_name="group_user")
    logical_team = models.ManyToManyField(Logical_team, verbose_name="Logické týmy", blank=True,
                                          related_name="group_logical_team", db_index=True)

    class Meta:
        verbose_name = "Skupina"
        verbose_name_plural = 'Skupiny'
        ordering = ["name"]

        def __unicode__(self):
            return self.verbose_name


TreeForeignKey(Group, blank=True, null=True, db_index=True, on_delete=models.CASCADE,
               verbose_name="Nadřazená skupina").contribute_to_class(Group, 'parent')
mptt.register(Group, order_insertion_on=["name"])


class User_external_id(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, blank=True, db_index=True,
                                verbose_name="Uživatel")
    oc = models.CharField("Externí ID v LDAP", blank=False, null=True, db_index=True, max_length=6)

    def create_user_external_id(sender, instance, created, **kwargs):
        if created:
            new_profile = User_external_id.objects.create(user=instance)
            user = LDAPBackend().populate_user(instance.username)
            if user:
                oc = user.ldap_user.attrs.get("physicalDeliveryOfficeName", [])[0]
                new_profile.oc = oc
                new_profile.save()

    post_save.connect(create_user_external_id, sender=User)

    def __str__(self):
        return u"{}".format(self.user.username)


@receiver(post_save, sender=User)
def create_or_update_user_profile(sender, instance, created, **kwargs):
    if created:
        User_external_id.objects.create(user=instance)
        instance.user_external_id.save()


class User_logical_team(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, blank=True, db_index=True)
    team = models.ForeignKey(Logical_team, on_delete=models.CASCADE, null=True, blank=True, db_index=True,
                             verbose_name="Primární logický tým")


@receiver(post_save, sender=User)
def create_or_update_user_profile(sender, instance, created, **kwargs):
    if created:
        User_logical_team.objects.create(user=instance)
        instance.user_logical_team.save()


class User_contract(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, blank=True, db_index=True)
    contract = models.DecimalField(verbose_name="Velikost Úvazku", decimal_places=2, max_digits=30, null=True,
                                   blank=True, )


@receiver(post_save, sender=User)
def create_or_update_user_profile(sender, instance, created, **kwargs):
    if created:
        User_contract.objects.create(user=instance)
        instance.user_contract.save()


class Status_activity(models.Model):
    name = models.CharField('Název stavu', max_length=200)

    class Meta:
        verbose_name = 'Možné stavy aktivit'
        verbose_name_plural = 'Možné stavy aktivit'

    def __str__(self):
        return self.name


class Anet_activity_type(models.Model):
    name = models.CharField('Jméno', max_length=200)
    capitalized = models.BooleanField("Proplacené", default=True)
    external_id = models.PositiveIntegerField("ID v systému ANET", blank=False, db_index=True)

    class Meta:
        verbose_name = 'ANET kategorií'
        verbose_name_plural = 'ANET kategorie'

    def __str__(self):
        return self.name


class Template_activity_type(models.Model):
    name = models.CharField('Jméno', max_length=200, db_index=True)
    anet_category = models.ManyToManyField(Anet_activity_type, verbose_name="Aktivity v ANETu", blank=True,
                                           db_index=True)

    class Meta:
        verbose_name = 'Typ aktivity'
        verbose_name_plural = 'Typy aktivit'

    def __str__(self):
        return self.name


class Activity_subtype(models.Model):
    name = models.CharField('Jméno', max_length=200)
    description = description = models.TextField('Popisek', blank=True, null=True, max_length=2000)

    class Meta:
        verbose_name = 'Aktivity - combobox'
        verbose_name_plural = 'Aktivita - combobox'

    def __str__(self):
        return self.name


class Template_activity(models.Model):
    name = models.CharField("Název šablony", max_length=200, db_index=True)
    description = models.TextField("Popisek (nepovinné)", blank=True, max_length=2000)
    avg_time = models.TimeField(blank=True, default="00:00:00", verbose_name="Průměrný čas")
    id_category = models.ForeignKey(Category_activity, on_delete=models.CASCADE, db_index=True, verbose_name="Záložka")
    icon = IconField(verbose_name="Ikona")
    support_activity = models.BooleanField('Podpůrná aktivita', default=False, db_index=True)
    template_type = models.ForeignKey(Template_activity_type, on_delete=models.CASCADE, verbose_name="Typy aktivity",
                                      db_index=True)
    alow_subtype = models.BooleanField('Specifikace aktivity - combo', default=False)
    priority = models.IntegerField(default=0, verbose_name="Priorita - vyšší číslo = větší")
    modules = models.ManyToManyField(Osvald_modules, verbose_name="Moduly", blank=True)

    class Meta:
        verbose_name = 'Dlaždice'
        verbose_name_plural = 'Dlaždice'

    def __str__(self):
        return self.name


class Avg_activity(models.Model):
    template = models.ForeignKey(Template_activity, on_delete=models.CASCADE)
    date = models.ForeignKey(Logical_team, on_delete=models.CASCADE)
    avg_time = models.DateTimeField(default=datetime.now, blank=True, db_index=True)


class Activity(models.Model):
    name = models.CharField(max_length=200, blank=True)
    comment = models.TextField(blank=True)
    template = models.ForeignKey(Template_activity,
                                 on_delete=models.CASCADE, blank=True, db_index=True)
    subtype = models.ForeignKey(Activity_subtype, on_delete=models.CASCADE,blank=True,null=True)
    status = models.ForeignKey(Status_activity, on_delete=models.CASCADE, blank=True, db_index=True)
    customer_id = models.CharField(max_length=200, blank=True)
    document_id = models.CharField(max_length=200, blank=True)
    username = models.ForeignKey(User, on_delete=models.CASCADE, db_index=True)
    created_at = models.DateTimeField(default=datetime.now, blank=True, db_index=True)
    updated_at = models.DateTimeField(blank=True, null=True, db_index=True)
    total_time = models.CharField(max_length=200, blank=True)
    activity_count = models.PositiveIntegerField(blank=True, default=0)
    total_seconds = models.PositiveIntegerField(blank=True, default=0)

    def save(self):
        if self.created_at == None:
            self.created_at = datetime.now(tz=timezone.utc)
        self.updated_at = datetime.now(tz=timezone.utc)
        self.total_seconds = int(
            int(self.total_time.split(":")[0].strip()) * 3600 + int(self.total_time.split(":")[1].strip()) * 60 + int(
                self.total_time.split(":")[2].strip()))
        self.name = "Activita-"+str(self.username)+"-"+str(self.template_id)+"-"+str(self.created_at.strftime('%Y%m%d%H%M%S'))
        super(Activity, self).save()

    def __str__(self):
        return self.name


class Anet_activity(models.Model):
    id_ucet = models.ForeignKey(Anet_activity_type, on_delete=models.CASCADE, blank=False, null=False,
                                verbose_name="ANET Status")
    oc = models.ForeignKey(User, on_delete=models.CASCADE, blank=False, null=False, verbose_name="Uživatel",
                           db_index=True)
    minuty = models.IntegerField(blank=False, null=False, verbose_name="Počet minut", db_index=True)
    datum = models.DateField(blank=True, null=True, verbose_name="Datum")
    dny = models.IntegerField(blank=True, null=True, verbose_name="Celý den")

    #
    class Meta:
        verbose_name = 'ANET import'
        verbose_name_plural = 'ANET import'


class Tmp_user_metadata(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, db_index=True)
    date = models.DateField(blank=True, null=True, db_index=True)
    cisty_cas = models.DecimalField(verbose_name="Čistý čas", decimal_places=4, max_digits=8)
    fond = models.DecimalField(decimal_places=4, max_digits=8)
    efektivni = models.DecimalField(decimal_places=4, max_digits=8)
    ztrata = models.DecimalField(decimal_places=4, max_digits=8)
    nepritomnost = models.DecimalField(decimal_places=4, max_digits=8)
    podpurne_anet = models.DecimalField(decimal_places=4, max_digits=8)
    podpurne_dlazdice = models.DecimalField(decimal_places=4, max_digits=8)
