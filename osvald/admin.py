from django.contrib import admin
from django.contrib import auth
from django.contrib.auth.models import User,AbstractUser
from .models import Category_activity
from .models import Logical_team,User_logical_team, Group,User_external_id
from .models import Template_activity,Template_activity_type
from .models import Status_activity, Activity_subtype
from .models import Anet_activity_type,Anet_activity
from .models import Osvald_modules,Selector_type_list,Selector_group,Activity_selector
from import_export.admin import ImportExportMixin, ImportExportModelAdmin,ImportMixin
from import_export import resources
from import_export import fields
from django.contrib.auth.forms import UserChangeForm

from import_export.widgets import ForeignKeyWidget,IntegerWidget,DateWidget,Widget
from .forms import DataImport
from django.contrib.auth.admin import UserAdmin,GroupAdmin
from mptt.admin import DraggableMPTTAdmin
from django.apps import apps
import tablib
import xlrd
from import_export.formats.base_formats import TablibFormat
from xlrd import xldate_as_tuple
import datetime



admin.site.register(Logical_team)
admin.site.register(Category_activity)
admin.site.register(Template_activity)
admin.site.register(Template_activity_type)
admin.site.register(Status_activity)
admin.site.register(Osvald_modules)
admin.site.register(Selector_type_list)
admin.site.register(Selector_group)
admin.site.register(Activity_selector)
from django.contrib.auth.models import Permission
admin.site.register(Anet_activity_type)

admin.site.register(Activity_subtype)
admin.site.unregister(auth.models.Group)


class CustomGroupAdmin(GroupAdmin):
    def formfield_for_manytomany(self, db_field, request=None, **kwargs):
        if db_field.name == 'permissions':
            qs = kwargs.get('queryset', db_field.remote_field.model.objects)
            qs = qs.exclude(codename__in=(
                'add_permission',
                'change_permission',
                'delete_permission',

                'add_contenttype',
                'change_contenttype',
                'delete_contenttype',

                'add_session',
                'delete_session',
                'change_session',

                'add_logentry',
                'change_logentry',
                'delete_logentry',
            ))
            # Avoid a major performance hit resolving permission names which
            # triggers a content_type load:
            kwargs['queryset'] = qs.select_related('content_type')
        return super(GroupAdmin, self).formfield_for_manytomany(
            db_field, request=request, **kwargs)


admin.site.register(
    Group,
    DraggableMPTTAdmin,
    filter_horizontal=('manager','user','logical_team'),
    list_display=(
        'tree_actions',
        'indented_title',
    ),
    list_display_links=(
        'indented_title',
    ),
exclude = ('permissions',),
)




class LogicalTeamInline(admin.StackedInline):
    model = User_logical_team
    can_delete = False
    verbose_name_plural = 'Logické týmy'
    fk_name = 'user'

class ExternalIDInline(admin.StackedInline):
    model = User_external_id
    can_delete = False
    verbose_name_plural = 'Externí ID'
    fk_name = 'user'

class CustomUserAdmin(UserAdmin):
    inlines = (LogicalTeamInline,ExternalIDInline, )

    def get_inline_instances(self, request, obj=None):
        if not obj:
            return list()
        return super(CustomUserAdmin, self).get_inline_instances(request, obj)

    list_filter = ('is_staff','is_active',)

    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
        ((''), {'fields': ('is_active', 'is_staff',
                                       )}),
    )

    class Meta:
        label = 'Osvald'

admin.site.unregister(User)
admin.site.register(User, CustomUserAdmin)


class Logical_teamAdmin(admin.ModelAdmin):
    filter_horizontal = ('user','category')
admin.site.unregister(Logical_team)
admin.site.register(Logical_team, Logical_teamAdmin)

class Osvald_modulesAdmin(admin.ModelAdmin):
    filter_horizontal = ('user',)
    list_display = ('name', )
admin.site.unregister(Osvald_modules)
admin.site.register(Osvald_modules, Osvald_modulesAdmin)

class Template_activity_typeAdmin(admin.ModelAdmin):
    filter_horizontal = ('anet_category',)


admin.site.unregister(Template_activity_type)
admin.site.register(Template_activity_type, Template_activity_typeAdmin)


class Template_activityAdmin(admin.ModelAdmin):

    list_display = ('name','id_category','support_activity','template_type','priority',)
    list_filter = ('id_category','support_activity','template_type',)


admin.site.unregister(Template_activity)
admin.site.register(Template_activity, Template_activityAdmin)



class AnetResource(resources.ModelResource):
    minuty = fields.Field(column_name="minuty",attribute="minuty")
    datum = fields.Field(column_name="den",attribute="datum",widget=DateWidget("%d.%m.%Y"))
    id_ucet = fields.Field(column_name="id_ucet",attribute="id_ucet", widget=ForeignKeyWidget(Anet_activity_type, 'external_id'))
    dny = fields.Field(column_name="dny",attribute="dny")
    oc = fields.Field(column_name="oc", attribute="oc",widget=ForeignKeyWidget(User))

    def before_import(self, dataset, *args, **kwargs):
        if dataset.headers:
            dataset.headers = [str(header).lower().strip() for header in dataset.headers]
          # if id column not in headers in your file
        if 'id' not in dataset.headers:
            dataset.headers.append('id')

    def before_import_row(self,row, **kwargs):
        value = row['oc']
        dat = row['den']

        obj = User_external_id.objects.get(oc=value) #create object place
        row['den'] = dat.strftime('%d.%m.%Y')

        row['oc'] = obj.user.id # update value to id ob new object


    def get_instance(self, instance_loader, row):
        return False

    class Meta:
        model = Anet_activity

        fields =('datum','minuty','id_ucet','oc')




        #def before_import(self, dataset, dry_run):
        #    if dataset.headers:
        #        dataset.headers = [str(header).lower().strip() for header in dataset.headers]
        #    # if you already have a id column in your file with values for this column then don't use below code

class AnetAdmin(ImportMixin, admin.ModelAdmin):
    def hodin(self, obj):
        return str(obj.minuty/60)
    def saldo(self,obj):
        return str(str(int(obj.dny)*100) + "%")

    def test(self, obj):
        return str(obj.minuty/60)

    list_display = ('datum','oc')

    # def queryset(self, request):
    #     qs = super(AnetResource, self).queryset(request)
    #     qs = qs.distinct('datum','oc')
    #     return qs
    search_fields = ['oc']
    list_display = ('datum','minuty','hodin','oc','id_ucet')
    list_display_links = ('oc','datum' )
    list_filter = ('id_ucet', 'oc')
    resource_class = AnetResource


admin.site.register(Anet_activity, AnetAdmin)


