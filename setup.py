from setuptools import setup

setup(
    name='django_osvald',
    version='',
    packages=['venv1.lib.python3.6.distutils', 'venv1.lib.python3.6.encodings', 'venv1.lib.python3.6.importlib',
              'venv1.lib.python3.6.collections', 'venv1.lib.python3.6.site-packages.fa',
              'venv1.lib.python3.6.site-packages.fa.templatetags', 'venv1.lib.python3.6.site-packages.odf',
              'venv1.lib.python3.6.site-packages.pip', 'venv1.lib.python3.6.site-packages.pip.req',
              'venv1.lib.python3.6.site-packages.pip.vcs', 'venv1.lib.python3.6.site-packages.pip.utils',
              'venv1.lib.python3.6.site-packages.pip.compat', 'venv1.lib.python3.6.site-packages.pip.models',
              'venv1.lib.python3.6.site-packages.pip._vendor', 'venv1.lib.python3.6.site-packages.pip._vendor.distlib',
              'venv1.lib.python3.6.site-packages.pip._vendor.distlib._backport',
              'venv1.lib.python3.6.site-packages.pip._vendor.colorama',
              'venv1.lib.python3.6.site-packages.pip._vendor.html5lib',
              'venv1.lib.python3.6.site-packages.pip._vendor.html5lib._trie',
              'venv1.lib.python3.6.site-packages.pip._vendor.html5lib.filters',
              'venv1.lib.python3.6.site-packages.pip._vendor.html5lib.treewalkers',
              'venv1.lib.python3.6.site-packages.pip._vendor.html5lib.treeadapters',
              'venv1.lib.python3.6.site-packages.pip._vendor.html5lib.treebuilders',
              'venv1.lib.python3.6.site-packages.pip._vendor.lockfile',
              'venv1.lib.python3.6.site-packages.pip._vendor.progress',
              'venv1.lib.python3.6.site-packages.pip._vendor.requests',
              'venv1.lib.python3.6.site-packages.pip._vendor.requests.packages',
              'venv1.lib.python3.6.site-packages.pip._vendor.requests.packages.chardet',
              'venv1.lib.python3.6.site-packages.pip._vendor.requests.packages.urllib3',
              'venv1.lib.python3.6.site-packages.pip._vendor.requests.packages.urllib3.util',
              'venv1.lib.python3.6.site-packages.pip._vendor.requests.packages.urllib3.contrib',
              'venv1.lib.python3.6.site-packages.pip._vendor.requests.packages.urllib3.packages',
              'venv1.lib.python3.6.site-packages.pip._vendor.requests.packages.urllib3.packages.ssl_match_hostname',
              'venv1.lib.python3.6.site-packages.pip._vendor.packaging',
              'venv1.lib.python3.6.site-packages.pip._vendor.cachecontrol',
              'venv1.lib.python3.6.site-packages.pip._vendor.cachecontrol.caches',
              'venv1.lib.python3.6.site-packages.pip._vendor.webencodings',
              'venv1.lib.python3.6.site-packages.pip._vendor.pkg_resources',
              'venv1.lib.python3.6.site-packages.pip.commands', 'venv1.lib.python3.6.site-packages.pip.operations',
              'venv1.lib.python3.6.site-packages.mptt', 'venv1.lib.python3.6.site-packages.mptt.templatetags',
              'venv1.lib.python3.6.site-packages.pytz', 'venv1.lib.python3.6.site-packages.suit',
              'venv1.lib.python3.6.site-packages.suit.templatetags', 'venv1.lib.python3.6.site-packages.xlrd',
              'venv1.lib.python3.6.site-packages.xlwt', 'venv1.lib.python3.6.site-packages.yaml',
              'venv1.lib.python3.6.site-packages.cache', 'venv1.lib.python3.6.site-packages.wheel',
              'venv1.lib.python3.6.site-packages.wheel.tool', 'venv1.lib.python3.6.site-packages.wheel.signatures',
              'venv1.lib.python3.6.site-packages.braces', 'venv1.lib.python3.6.site-packages.braces.views',
              'venv1.lib.python3.6.site-packages.django', 'venv1.lib.python3.6.site-packages.django.db',
              'venv1.lib.python3.6.site-packages.django.db.models',
              'venv1.lib.python3.6.site-packages.django.db.models.sql',
              'venv1.lib.python3.6.site-packages.django.db.models.fields',
              'venv1.lib.python3.6.site-packages.django.db.models.functions',
              'venv1.lib.python3.6.site-packages.django.db.backends',
              'venv1.lib.python3.6.site-packages.django.db.backends.base',
              'venv1.lib.python3.6.site-packages.django.db.backends.dummy',
              'venv1.lib.python3.6.site-packages.django.db.backends.mysql',
              'venv1.lib.python3.6.site-packages.django.db.backends.oracle',
              'venv1.lib.python3.6.site-packages.django.db.backends.sqlite3',
              'venv1.lib.python3.6.site-packages.django.db.backends.postgresql',
              'venv1.lib.python3.6.site-packages.django.db.backends.postgresql_psycopg2',
              'venv1.lib.python3.6.site-packages.django.db.migrations',
              'venv1.lib.python3.6.site-packages.django.db.migrations.operations',
              'venv1.lib.python3.6.site-packages.django.apps', 'venv1.lib.python3.6.site-packages.django.conf',
              'venv1.lib.python3.6.site-packages.django.conf.urls',
              'venv1.lib.python3.6.site-packages.django.conf.locale',
              'venv1.lib.python3.6.site-packages.django.conf.locale.ar',
              'venv1.lib.python3.6.site-packages.django.conf.locale.az',
              'venv1.lib.python3.6.site-packages.django.conf.locale.bg',
              'venv1.lib.python3.6.site-packages.django.conf.locale.bn',
              'venv1.lib.python3.6.site-packages.django.conf.locale.bs',
              'venv1.lib.python3.6.site-packages.django.conf.locale.ca',
              'venv1.lib.python3.6.site-packages.django.conf.locale.cs',
              'venv1.lib.python3.6.site-packages.django.conf.locale.cy',
              'venv1.lib.python3.6.site-packages.django.conf.locale.da',
              'venv1.lib.python3.6.site-packages.django.conf.locale.de',
              'venv1.lib.python3.6.site-packages.django.conf.locale.el',
              'venv1.lib.python3.6.site-packages.django.conf.locale.en',
              'venv1.lib.python3.6.site-packages.django.conf.locale.eo',
              'venv1.lib.python3.6.site-packages.django.conf.locale.es',
              'venv1.lib.python3.6.site-packages.django.conf.locale.et',
              'venv1.lib.python3.6.site-packages.django.conf.locale.eu',
              'venv1.lib.python3.6.site-packages.django.conf.locale.fa',
              'venv1.lib.python3.6.site-packages.django.conf.locale.fi',
              'venv1.lib.python3.6.site-packages.django.conf.locale.fr',
              'venv1.lib.python3.6.site-packages.django.conf.locale.fy',
              'venv1.lib.python3.6.site-packages.django.conf.locale.ga',
              'venv1.lib.python3.6.site-packages.django.conf.locale.gd',
              'venv1.lib.python3.6.site-packages.django.conf.locale.gl',
              'venv1.lib.python3.6.site-packages.django.conf.locale.he',
              'venv1.lib.python3.6.site-packages.django.conf.locale.hi',
              'venv1.lib.python3.6.site-packages.django.conf.locale.hr',
              'venv1.lib.python3.6.site-packages.django.conf.locale.hu',
              'venv1.lib.python3.6.site-packages.django.conf.locale.id',
              'venv1.lib.python3.6.site-packages.django.conf.locale.is',
              'venv1.lib.python3.6.site-packages.django.conf.locale.it',
              'venv1.lib.python3.6.site-packages.django.conf.locale.ja',
              'venv1.lib.python3.6.site-packages.django.conf.locale.ka',
              'venv1.lib.python3.6.site-packages.django.conf.locale.km',
              'venv1.lib.python3.6.site-packages.django.conf.locale.kn',
              'venv1.lib.python3.6.site-packages.django.conf.locale.ko',
              'venv1.lib.python3.6.site-packages.django.conf.locale.lt',
              'venv1.lib.python3.6.site-packages.django.conf.locale.lv',
              'venv1.lib.python3.6.site-packages.django.conf.locale.mk',
              'venv1.lib.python3.6.site-packages.django.conf.locale.ml',
              'venv1.lib.python3.6.site-packages.django.conf.locale.mn',
              'venv1.lib.python3.6.site-packages.django.conf.locale.nb',
              'venv1.lib.python3.6.site-packages.django.conf.locale.nl',
              'venv1.lib.python3.6.site-packages.django.conf.locale.nn',
              'venv1.lib.python3.6.site-packages.django.conf.locale.pl',
              'venv1.lib.python3.6.site-packages.django.conf.locale.pt',
              'venv1.lib.python3.6.site-packages.django.conf.locale.ro',
              'venv1.lib.python3.6.site-packages.django.conf.locale.ru',
              'venv1.lib.python3.6.site-packages.django.conf.locale.sk',
              'venv1.lib.python3.6.site-packages.django.conf.locale.sl',
              'venv1.lib.python3.6.site-packages.django.conf.locale.sq',
              'venv1.lib.python3.6.site-packages.django.conf.locale.sr',
              'venv1.lib.python3.6.site-packages.django.conf.locale.sv',
              'venv1.lib.python3.6.site-packages.django.conf.locale.ta',
              'venv1.lib.python3.6.site-packages.django.conf.locale.te',
              'venv1.lib.python3.6.site-packages.django.conf.locale.th',
              'venv1.lib.python3.6.site-packages.django.conf.locale.tr',
              'venv1.lib.python3.6.site-packages.django.conf.locale.uk',
              'venv1.lib.python3.6.site-packages.django.conf.locale.vi',
              'venv1.lib.python3.6.site-packages.django.conf.locale.de_CH',
              'venv1.lib.python3.6.site-packages.django.conf.locale.en_AU',
              'venv1.lib.python3.6.site-packages.django.conf.locale.en_GB',
              'venv1.lib.python3.6.site-packages.django.conf.locale.es_AR',
              'venv1.lib.python3.6.site-packages.django.conf.locale.es_CO',
              'venv1.lib.python3.6.site-packages.django.conf.locale.es_MX',
              'venv1.lib.python3.6.site-packages.django.conf.locale.es_NI',
              'venv1.lib.python3.6.site-packages.django.conf.locale.es_PR',
              'venv1.lib.python3.6.site-packages.django.conf.locale.pt_BR',
              'venv1.lib.python3.6.site-packages.django.conf.locale.sr_Latn',
              'venv1.lib.python3.6.site-packages.django.conf.locale.zh_Hans',
              'venv1.lib.python3.6.site-packages.django.conf.locale.zh_Hant',
              'venv1.lib.python3.6.site-packages.django.core', 'venv1.lib.python3.6.site-packages.django.core.mail',
              'venv1.lib.python3.6.site-packages.django.core.mail.backends',
              'venv1.lib.python3.6.site-packages.django.core.cache',
              'venv1.lib.python3.6.site-packages.django.core.cache.backends',
              'venv1.lib.python3.6.site-packages.django.core.files',
              'venv1.lib.python3.6.site-packages.django.core.checks',
              'venv1.lib.python3.6.site-packages.django.core.checks.security',
              'venv1.lib.python3.6.site-packages.django.core.checks.compatibility',
              'venv1.lib.python3.6.site-packages.django.core.servers',
              'venv1.lib.python3.6.site-packages.django.core.handlers',
              'venv1.lib.python3.6.site-packages.django.core.management',
              'venv1.lib.python3.6.site-packages.django.core.management.commands',
              'venv1.lib.python3.6.site-packages.django.core.serializers',
              'venv1.lib.python3.6.site-packages.django.http', 'venv1.lib.python3.6.site-packages.django.test',
              'venv1.lib.python3.6.site-packages.django.urls', 'venv1.lib.python3.6.site-packages.django.forms',
              'venv1.lib.python3.6.site-packages.django.utils',
              'venv1.lib.python3.6.site-packages.django.utils.translation',
              'venv1.lib.python3.6.site-packages.django.views',
              'venv1.lib.python3.6.site-packages.django.views.generic',
              'venv1.lib.python3.6.site-packages.django.views.decorators',
              'venv1.lib.python3.6.site-packages.django.contrib',
              'venv1.lib.python3.6.site-packages.django.contrib.gis',
              'venv1.lib.python3.6.site-packages.django.contrib.gis.db',
              'venv1.lib.python3.6.site-packages.django.contrib.gis.db.models',
              'venv1.lib.python3.6.site-packages.django.contrib.gis.db.models.sql',
              'venv1.lib.python3.6.site-packages.django.contrib.gis.db.backends',
              'venv1.lib.python3.6.site-packages.django.contrib.gis.db.backends.base',
              'venv1.lib.python3.6.site-packages.django.contrib.gis.db.backends.mysql',
              'venv1.lib.python3.6.site-packages.django.contrib.gis.db.backends.oracle',
              'venv1.lib.python3.6.site-packages.django.contrib.gis.db.backends.postgis',
              'venv1.lib.python3.6.site-packages.django.contrib.gis.db.backends.spatialite',
              'venv1.lib.python3.6.site-packages.django.contrib.gis.gdal',
              'venv1.lib.python3.6.site-packages.django.contrib.gis.gdal.raster',
              'venv1.lib.python3.6.site-packages.django.contrib.gis.gdal.prototypes',
              'venv1.lib.python3.6.site-packages.django.contrib.gis.geos',
              'venv1.lib.python3.6.site-packages.django.contrib.gis.geos.prototypes',
              'venv1.lib.python3.6.site-packages.django.contrib.gis.admin',
              'venv1.lib.python3.6.site-packages.django.contrib.gis.forms',
              'venv1.lib.python3.6.site-packages.django.contrib.gis.utils',
              'venv1.lib.python3.6.site-packages.django.contrib.gis.geoip2',
              'venv1.lib.python3.6.site-packages.django.contrib.gis.sitemaps',
              'venv1.lib.python3.6.site-packages.django.contrib.gis.management',
              'venv1.lib.python3.6.site-packages.django.contrib.gis.management.commands',
              'venv1.lib.python3.6.site-packages.django.contrib.gis.serializers',
              'venv1.lib.python3.6.site-packages.django.contrib.auth',
              'venv1.lib.python3.6.site-packages.django.contrib.auth.handlers',
              'venv1.lib.python3.6.site-packages.django.contrib.auth.management',
              'venv1.lib.python3.6.site-packages.django.contrib.auth.management.commands',
              'venv1.lib.python3.6.site-packages.django.contrib.auth.migrations',
              'venv1.lib.python3.6.site-packages.django.contrib.admin',
              'venv1.lib.python3.6.site-packages.django.contrib.admin.views',
              'venv1.lib.python3.6.site-packages.django.contrib.admin.migrations',
              'venv1.lib.python3.6.site-packages.django.contrib.admin.templatetags',
              'venv1.lib.python3.6.site-packages.django.contrib.sites',
              'venv1.lib.python3.6.site-packages.django.contrib.sites.migrations',
              'venv1.lib.python3.6.site-packages.django.contrib.humanize',
              'venv1.lib.python3.6.site-packages.django.contrib.humanize.templatetags',
              'venv1.lib.python3.6.site-packages.django.contrib.messages',
              'venv1.lib.python3.6.site-packages.django.contrib.messages.storage',
              'venv1.lib.python3.6.site-packages.django.contrib.postgres',
              'venv1.lib.python3.6.site-packages.django.contrib.postgres.forms',
              'venv1.lib.python3.6.site-packages.django.contrib.postgres.fields',
              'venv1.lib.python3.6.site-packages.django.contrib.postgres.aggregates',
              'venv1.lib.python3.6.site-packages.django.contrib.sessions',
              'venv1.lib.python3.6.site-packages.django.contrib.sessions.backends',
              'venv1.lib.python3.6.site-packages.django.contrib.sessions.management',
              'venv1.lib.python3.6.site-packages.django.contrib.sessions.management.commands',
              'venv1.lib.python3.6.site-packages.django.contrib.sessions.migrations',
              'venv1.lib.python3.6.site-packages.django.contrib.sitemaps',
              'venv1.lib.python3.6.site-packages.django.contrib.sitemaps.management',
              'venv1.lib.python3.6.site-packages.django.contrib.sitemaps.management.commands',
              'venv1.lib.python3.6.site-packages.django.contrib.admindocs',
              'venv1.lib.python3.6.site-packages.django.contrib.flatpages',
              'venv1.lib.python3.6.site-packages.django.contrib.flatpages.migrations',
              'venv1.lib.python3.6.site-packages.django.contrib.flatpages.templatetags',
              'venv1.lib.python3.6.site-packages.django.contrib.redirects',
              'venv1.lib.python3.6.site-packages.django.contrib.redirects.migrations',
              'venv1.lib.python3.6.site-packages.django.contrib.staticfiles',
              'venv1.lib.python3.6.site-packages.django.contrib.staticfiles.management',
              'venv1.lib.python3.6.site-packages.django.contrib.staticfiles.management.commands',
              'venv1.lib.python3.6.site-packages.django.contrib.staticfiles.templatetags',
              'venv1.lib.python3.6.site-packages.django.contrib.syndication',
              'venv1.lib.python3.6.site-packages.django.contrib.contenttypes',
              'venv1.lib.python3.6.site-packages.django.contrib.contenttypes.management',
              'venv1.lib.python3.6.site-packages.django.contrib.contenttypes.management.commands',
              'venv1.lib.python3.6.site-packages.django.contrib.contenttypes.migrations',
              'venv1.lib.python3.6.site-packages.django.dispatch', 'venv1.lib.python3.6.site-packages.django.template',
              'venv1.lib.python3.6.site-packages.django.template.loaders',
              'venv1.lib.python3.6.site-packages.django.template.backends',
              'venv1.lib.python3.6.site-packages.django.middleware',
              'venv1.lib.python3.6.site-packages.django.templatetags', 'venv1.lib.python3.6.site-packages.tablib',
              'venv1.lib.python3.6.site-packages.tablib.formats', 'venv1.lib.python3.6.site-packages.tablib.packages',
              'venv1.lib.python3.6.site-packages.tablib.packages.dbfpy',
              'venv1.lib.python3.6.site-packages.tablib.packages.dbfpy3', 'venv1.lib.python3.6.site-packages.chardet',
              'venv1.lib.python3.6.site-packages.chardet.cli', 'venv1.lib.python3.6.site-packages.chartit',
              'venv1.lib.python3.6.site-packages.chartit.templatetags', 'venv1.lib.python3.6.site-packages.chartjs',
              'venv1.lib.python3.6.site-packages.chartjs.views', 'venv1.lib.python3.6.site-packages.graphos',
              'venv1.lib.python3.6.site-packages.graphos.sources',
              'venv1.lib.python3.6.site-packages.graphos.renderers',
              'venv1.lib.python3.6.site-packages.graphos.templatetags', 'venv1.lib.python3.6.site-packages.MySQLdb',
              'venv1.lib.python3.6.site-packages.MySQLdb.constants', 'venv1.lib.python3.6.site-packages.slugify',
              'venv1.lib.python3.6.site-packages.guardian', 'venv1.lib.python3.6.site-packages.guardian.conf',
              'venv1.lib.python3.6.site-packages.guardian.testapp',
              'venv1.lib.python3.6.site-packages.guardian.testapp.tests',
              'venv1.lib.python3.6.site-packages.guardian.testapp.migrations',
              'venv1.lib.python3.6.site-packages.guardian.management',
              'venv1.lib.python3.6.site-packages.guardian.management.commands',
              'venv1.lib.python3.6.site-packages.guardian.migrations',
              'venv1.lib.python3.6.site-packages.guardian.templatetags', 'venv1.lib.python3.6.site-packages.js_asset',
              'venv1.lib.python3.6.site-packages.material', 'venv1.lib.python3.6.site-packages.material.admin',
              'venv1.lib.python3.6.site-packages.material.theme',
              'venv1.lib.python3.6.site-packages.material.theme.red',
              'venv1.lib.python3.6.site-packages.material.theme.blue',
              'venv1.lib.python3.6.site-packages.material.theme.cyan',
              'venv1.lib.python3.6.site-packages.material.theme.lime',
              'venv1.lib.python3.6.site-packages.material.theme.pink',
              'venv1.lib.python3.6.site-packages.material.theme.teal',
              'venv1.lib.python3.6.site-packages.material.theme.amber',
              'venv1.lib.python3.6.site-packages.material.theme.brown',
              'venv1.lib.python3.6.site-packages.material.theme.green',
              'venv1.lib.python3.6.site-packages.material.theme.indigo',
              'venv1.lib.python3.6.site-packages.material.theme.orange',
              'venv1.lib.python3.6.site-packages.material.theme.purple',
              'venv1.lib.python3.6.site-packages.material.theme.yellow',
              'venv1.lib.python3.6.site-packages.material.theme.bluegrey',
              'venv1.lib.python3.6.site-packages.material.theme.lightblue',
              'venv1.lib.python3.6.site-packages.material.theme.deeporange',
              'venv1.lib.python3.6.site-packages.material.theme.deeppurple',
              'venv1.lib.python3.6.site-packages.material.theme.lightgreen',
              'venv1.lib.python3.6.site-packages.material.frontend',
              'venv1.lib.python3.6.site-packages.material.frontend.views',
              'venv1.lib.python3.6.site-packages.material.frontend.management',
              'venv1.lib.python3.6.site-packages.material.frontend.management.commands',
              'venv1.lib.python3.6.site-packages.material.frontend.migrations',
              'venv1.lib.python3.6.site-packages.material.frontend.templatetags',
              'venv1.lib.python3.6.site-packages.material.templatetags', 'venv1.lib.python3.6.site-packages.openpyxl',
              'venv1.lib.python3.6.site-packages.openpyxl.xml', 'venv1.lib.python3.6.site-packages.openpyxl.cell',
              'venv1.lib.python3.6.site-packages.openpyxl.chart', 'venv1.lib.python3.6.site-packages.openpyxl.pivot',
              'venv1.lib.python3.6.site-packages.openpyxl.utils', 'venv1.lib.python3.6.site-packages.openpyxl.compat',
              'venv1.lib.python3.6.site-packages.openpyxl.reader', 'venv1.lib.python3.6.site-packages.openpyxl.styles',
              'venv1.lib.python3.6.site-packages.openpyxl.writer', 'venv1.lib.python3.6.site-packages.openpyxl.drawing',
              'venv1.lib.python3.6.site-packages.openpyxl.formula',
              'venv1.lib.python3.6.site-packages.openpyxl.comments',
              'venv1.lib.python3.6.site-packages.openpyxl.workbook',
              'venv1.lib.python3.6.site-packages.openpyxl.workbook.external_link',
              'venv1.lib.python3.6.site-packages.openpyxl.packaging',
              'venv1.lib.python3.6.site-packages.openpyxl.worksheet',
              'venv1.lib.python3.6.site-packages.openpyxl.chartsheet',
              'venv1.lib.python3.6.site-packages.openpyxl.formatting',
              'venv1.lib.python3.6.site-packages.openpyxl.descriptors',
              'venv1.lib.python3.6.site-packages.csvimport.tests',
              'venv1.lib.python3.6.site-packages.csvimport.management',
              'venv1.lib.python3.6.site-packages.csvimport.management.commands',
              'venv1.lib.python3.6.site-packages.csvimport.migrations', 'venv1.lib.python3.6.site-packages.jsonfield',
              'venv1.lib.python3.6.site-packages.unidecode', 'venv1.lib.python3.6.site-packages.bootstrap3',
              'venv1.lib.python3.6.site-packages.bootstrap3.templatetags',
              'venv1.lib.python3.6.site-packages.bootstrap4',
              'venv1.lib.python3.6.site-packages.bootstrap4.templatetags',
              'venv1.lib.python3.6.site-packages.colorfield', 'venv1.lib.python3.6.site-packages.et_xmlfile',
              'venv1.lib.python3.6.site-packages.et_xmlfile.tests', 'venv1.lib.python3.6.site-packages.setuptools',
              'venv1.lib.python3.6.site-packages.setuptools.extern',
              'venv1.lib.python3.6.site-packages.setuptools.command', 'venv1.lib.python3.6.site-packages.unicodecsv',
              'venv1.lib.python3.6.site-packages.fontawesome',
              'venv1.lib.python3.6.site-packages.fontawesome.templatetags',
              'venv1.lib.python3.6.site-packages.admin_import', 'venv1.lib.python3.6.site-packages.import_export',
              'venv1.lib.python3.6.site-packages.import_export.formats',
              'venv1.lib.python3.6.site-packages.import_export.templatetags',
              'venv1.lib.python3.6.site-packages.pkg_resources',
              'venv1.lib.python3.6.site-packages.pkg_resources.extern',
              'venv1.lib.python3.6.site-packages.pkg_resources._vendor',
              'venv1.lib.python3.6.site-packages.pkg_resources._vendor.packaging',
              'venv1.lib.python3.6.site-packages.groups_manager',
              'venv1.lib.python3.6.site-packages.groups_manager.migrations',
              'venv1.lib.python3.6.site-packages.groups_manager.templatetags',
              'venv1.lib.python3.6.site-packages.diff_match_patch', 'osvald', 'osvald.middleware', 'osvald.migrations',
              'osvald.osvald_views', 'django_osvald'],
    url='',
    license='',
    author='tp',
    author_email='',
    description=''
)
